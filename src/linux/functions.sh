#!/bin/bash
############################
#   ___                 ____             _
#  / _ \__   _____ _ __|  _ \  ___ _ __ | | ___  _   _ 
# | | | \ \ / / _ \ '__| | | |/ _ \ '_ \| |/ _ \| | | |
# | |_| |\ V /  __/ |  | |_| |  __/ |_) | | (_) | |_| |
#  \___/  \_/ \___|_|  |____/ \___| .__/|_|\___/ \__, |
#                                 |_|            |___/ 
############################

# starting with coloring the life & the funcs !
declare -A COLOR;
COLOR[STOP]='\e[0m';
COLOR[BOLD]='\e[1m';
COLOR[RED_LIGHT]='\e[1;31m';
COLOR[GREEN_LIGHT]='\e[1;38;5;84m';
COLOR[GREEN_DARK]='\e[1;38;5;82m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;229m';
COLOR[YELLOW_DARK]='\e[1;38;5;226m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;203m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';

NOCOLOR='\e[0m'
BOLD='\e[1m'
TICK="[${COLOR[GREEN_LIGHT]}✔${NOCOLOR}]"
CROSS="[${COLOR[RED_LIGHT]}✘${NOCOLOR}]"

# debugs
function debug_start() { set -x; }
function debug_end() { set +x; }

# fnEcho() - for multiple
#
#   ARGS :
#   - $1 :
#       - string echo text
#   - $2 :
#       - string 'check'
#       - string 'checked'
#       - string 'stage'
#   - $3 :
#       - string 'success'
#       - string 'fail'
#       - string 'wait'
#       - string 'nothing'
#       - string 'info'
#       - string 'start'
#       - string 'finish'
#
#   RETURN : string
#
function fnEcho() {
    if [[ -n $1 ]]; then
        if [[ -n $2 && $2 == "check"  ]]; then echo -ne "${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} [?] ${1}";
        elif [[ -n $2 && $2 == "checked" ]]; then
            if [[ -n $3 && $3 == "success" ]]; then echo -e "\\r\\033[K${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} ${TICK} ${1}";
            elif [[ -n $3 && $3 == "fail" ]]; then echo -e "\\r\\033[K${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} ${CROSS} ${1}";
            elif [[ -n $3 && $3 == "wait" ]]; then echo -e "\\r\\033[K${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} [!] ${1}";
            elif [[ -n $3 && $3 == "nothing" ]]; then echo -e "\\r\\033[K${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} [-] ${1}";
            elif [[ -n $3 && $3 == "info" ]]; then echo -e "\\r\\033[K${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} [i] ${1}"; fi;
        elif [[ -n $2 && $2 == "stage" ]]; then
            if [[ -n $3 && $3 == "start" ]]; then
                echo -e "\n${COLOR[ORANGE_DARK]}$(fnGetTextSlim) STARTING STAGE : '${1}' ...${NOCOLOR}\n"
            elif [[ -n $3 && $3 == "end" ]]; then
                echo -e "\n${COLOR[ORANGE_DARK]}$(fnGetTextSlim) ENDING STAGE : '${1}' finished at $(fnGetDate).${NOCOLOR}\n"
            fi
        else
            echo -e "${COLOR[ORANGE_DARK]}$(fnGetTextSlim)${NOCOLOR} ${1}"
        fi
    fi
}

# fnGetRoot() - for fnCheckRoot()
#
#   ARGS : no arg
#
#   RETURN : code
#
function fnGetRoot() { if [[ "$(id -u)" -eq 0 ]]; then return 0; else return 1; fi; }

# fnGetDate() - for multiple
#
#   ARGS :
#   - $1 :
#       - string 'full', 'backup' , 'sql' or null
#
#   RETURN : string
#
function fnGetDate() {
    local fullDate=$(date +"%A %d %B %Y - %Hh%M:%S");
    # SRC : https://stackoverflow.com/a/50806120/2704550
    local fullDate=( $fullDate );
    local fullDate=${fullDate[@]^};
    local backupDate=$(date +"%F_%H-%I");
    local slimDate=$(date +"%Hh%M:%S");
    local sqlDate=$(date +"%Y-%m-%d %H:%M:%S");
    if [[ -n "$1" && "$1" == "full" ]]; then
        echo $fullDate;
    elif [[ -n "$1" && "$1" == "backup" ]]; then
        echo $backupDate;
    elif [[ -n "$1" && "$1" == "sql" ]]; then
        echo $sqlDate;
    else
        echo $slimDate;
    fi;
}

# fnGetIP() - for fnSMBDConf()
#
#   ARGS : no arg
#
#   RETURN : string
#
function fnGetIP() {
    local ip=$(echo $varData | jq -r ".network.ip_check");
    if [ $(ip -o route get to $ip &> /dev/null | grep -q "RTNETLINK"; echo $?) ]; then
        echo $(hostname -I | sed -e 's/ .*//g');
    else
        echo $(ip -o route get to $ip | sed -n 's/.*src \([0-9.]\+\).*/\1/p');
    fi
}

# fnJoin() - for fnProgramsFeed()
#
#   ARGS :
#   - $1 :
#       - string separator
#   - $2 :
#       - string datas
#
#   RETURN : string
#
function fnJoin { local d=${1-}; local f=${2-}; if shift 2; then printf %s "$f" "${@/#/$d}"; fi; }

# fnGetDownload() - for multiple
#
#   ARGS :
#   - $1 :
#       - string 'wget' : classic download
#       - string 'curl' : special openvm-tools
#       - string 'git' : special fredos-datas git repository
#       - string 'check' : no download, just check
#   - $2 :
#       - string url ressource
#   - $3 :
#       - string path ressource
#   - $4 :
#       - string user:pass ressource
#
#   RETURN : code
#
function fnGetDownload() {
    # test : credentials
    if [[ -n "$4" && "$4" != "" && "$4" != "null" ]]; then
        local username=$(echo -e $4 | cut -d ':' -f 1)
        local password=$(echo -e $4 | cut -d ':' -f 2)
    fi
    # test : method
    if [[ "$1" == "wget" ]]; then
        if [[ -n "$4" && "$4" != "" && "$4" != "null" ]]; then
            if wget --timeout=10 --tries=2 --user "$username" --password "$password" -q "$2" -O "$3" &> /dev/null; then return 0; else return 1; fi;
        else
            if wget --timeout=10 --tries=2 -q "$2" -O "$3" &> /dev/null; then return 0; else return 1; fi;
        fi
    elif [[ "$1" == "curl" ]]; then
        if [[ -n "$4" && "$4" != "" && "$4" != "null" ]]; then
            if curl -s "$2" -u "${username}:${password}" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$3" -qi - &> /dev/null; then return 0; else return 1; fi;
        else
            if curl -s "$2" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$3" -qi - &> /dev/null; then return 0; else return 1; fi;
        fi
    elif [[ "$1" == "git" ]]; then
        if git clone --depth=1 "$2" "$3" --quiet &> /dev/null; then return 0; else return 1; fi;
    elif [[ "$1" == "check" ]]; then
        if [[ -n "$4" && "$4" != "" && "$4" != "null" ]]; then
            if wget --timeout=10 --tries=2 -q --user "$username" --password "$password" --spider "$2" &> /dev/null; then return 0; else return 1; fi;
        else
            if wget --timeout=10 --tries=2 -q --spider "$2" &> /dev/null; then return 0; else return 1; fi;
        fi
    else
        return 1
    fi
}

# fnGetConnect() - for fnCheckConnect()
#
#   ARGS : no arg
#
#   RETURN : code
#
function fnGetConnect() {
    local url=$(echo $varData | jq -r ".network.domain_check");
    if fnGetDownload "check" "$url" false; then return 0; else return 1; fi;
}

# fnCheckConnect() - for multiple
#
#   ARGS : no arg
#
#   RETURN : string + code
#
function fnCheckConnect() {
    fnEcho "checking network connection ..." "check"
    if fnGetConnect; then fnEcho "network available." "checked" "success"; else fnEcho "network not available. exit." "checked" "fail"; exit 1; fi;
}

# fnGetArg1() - for fnCheckArgs()
#
#   ARGS : 
#   - $1 :
#       - string url custom
#       - string nothing / 'auto' : si url pas précisée
#
#   RETURN : string
#
function fnGetArg1() { if [[ "$scriptArg1" == "auto" ]]; then echo "$scriptSrcJson"; else echo "$scriptArg1"; fi; }

# fnGetArg2() - for fnCheckArgs() & fnPrepareDeploy()
#
#   ARGS : 
#   - $1 :
#       - string nothing / 'auto' : si server pas de DE, si desktop pas de DE supplémentaire, checke si DE reconnu
#       - string 'server' : pas de DE quoi qu'il arrive
#       - string 'desktop' : si desktop checke DE si reconnu, sinon installe Gnome DE quoi qu'il arrive
#       - string 'gnome' : installe/check Gnome DE même si un autre DE est présent ou si server
#       - string 'kde' : installe/check Kde DE même si un autre DE est présent ou si server
#       - string 'xfce' : installe/check Xfce DE même si un autre DE est présent ou si server
#
#   RETURN : string or code
#
function fnGetArg2() {
    local array=("auto" "server" "desktop" "gnome" "kde" "xfce")
    for item in "${array[@]}"; do
        if [[ "$scriptArg2" == "$item" ]]; then echo "$scriptArg2"; break; fi;
    done
    return 1
}

# fnGetArg3() - for fnCheckArgs()
#
#   ARGS : 
#   - $1 :
#       - string nothing / 'auto' / 'hardcore' : installations + configurations
#       - string 'software' : installations
#       - string 'softcore' : configurations
#
#   RETURN : string or code
#
function fnGetArg3() {
    local array=("auto" "hardcore" "software" "softcore")
    for item in "${array[@]}"; do
        if [[ "$scriptArg3" == "$item" ]]; then echo "$scriptArg3"; break; fi;
    done
    return 1
}

# fnGetArg4() - for fnCheckArgs()
#
#   ARGS : 
#   - $1 :
#       - string priority custom
#
#   RETURN : string
#
function fnGetArg4() {
    if [[ -n "$scriptArg4" ]]; then echo "$scriptArg4"; else echo "0"; fi;
}

# fnGetArg5() - for fnCheckArgs()
#
#   ARGS : 
#   - $1 :
#       - string url custom
#       - string nothing / 'auto' : si url pas précisée
#
#   RETURN : string
#
function fnGetArg5() { if [[ "$scriptArg5" == "auto" ]]; then echo "$scriptSrcPool"; else echo "$scriptArg5"; fi; }

# fnGetUser() - for fnRecapMsg()
#
#   ARGS : no arg
#
#   RETURN : string
#
function fnGetUser() { echo $SUDO_USER; }

# fnGetTextSlim() - for fnEcho()
#
#   ARGS : no arg
#
#   RETURN : string
#
function fnGetTextSlim() { echo "Deploy ::"; }

# fnRemovePackage() - for fnPROGRAMS()
#
#   ARGS :
#   - $1 :
#       - string package name
#   - $2 :
#       - string 'native'
#       - string 'snap'
#       - string 'flat'
#
#   RETURN : code
#
function fnRemovePackage() {
    local package=${1}
    local source=${2}
    if [[ $source == "native" ]]; then
        if command -v apt-get &> /dev/null; then
            DEBIAN_FRONTEND=noninteractive apt-get remove -y ${package} &> /dev/null
            if dpkg-query -Wf'${db:Status-abbrev}' "${package}" 2>/dev/null | grep -q '^i'; then return 1; else return 0; fi;
        elif command -v dnf &> /dev/null; then
            dnf remove -y ${package} &> /dev/null
            if dnf -q list installed | grep "${package}"; then return 1; else return 0; fi;
        else
            return 1
        fi
    elif [[ $source == "snap" ]]; then
        if command -v snap &> /dev/null; then
            snap remove --purge ${package} &> /dev/null
            if snap list | grep "${package}"; then return 1; else return 0; fi;
        else
            return 1
        fi
    elif [[ $source == "flat" ]]; then
        if command -v flatpak &> /dev/null; then
            flatpak remove ${package} &> /dev/null
            if flatpak list | grep "${package}"; then return 1; else return 0; fi;
        else
            return 1
        fi
    else
        return 1
    fi
}

# fnCheckPrg() - for multiple
#
#   ARGS :
#   - $1 :
#       - string program name
#   - $2 :
#       - string package name
#
#   RETURN : string
#
function fnCheckPrg() {
    fnEcho "checking program ${1} ..." "check"
    if command -v $1 &> /dev/null; then
        fnEcho "program ${1} available." "checked" "success"
    else
        if [[ -n $2 ]]; then
            fnEcho "program ${1} (${2} package) not present and need installation because it's a dependencie." "checked" "wait"
            fnEcho "performing ${2} installation ..." "check"
            if fnInstallPackage "${2}" "native" false true false true; then
                fnEcho "${COLOR[YELLOW_LIGHT]}${2}${NOCOLOR} installed." "checked" "success"
            else
                fnEcho "${COLOR[YELLOW_LIGHT]}${2}${NOCOLOR} not installed and required. exit." "checked" "fail"; exit 1;
            fi
        else
            fnEcho "program ${1} not present but it's not a dependencie. pursuit." "checked" "nothing"
        fi
    fi
}

# fnCheckPackage() - for fnInstallPackage() & fnCHECKS()
#
#   ARGS :
#   - $1 :
#       - string package name
#
#   RETURN : string
#
function fnCheckPackage() {
    # .deb
    if command -v apt-get &> /dev/null; then
        # SRC : https://askubuntu.com/a/668229
        # previously used : << if dpkg-query -Wf'${db:Status-abbrev}' "${environnement}" 2>/dev/null | grep -q '^i'; >>
        if /usr/bin/dpkg-query --show --showformat="${db:Status-Status}\n" "$1" &> /dev/null; then return 0; else return 1; fi;
    # .rpm
    elif command -v dnf &> /dev/null; then
        if dnf -q list installed | grep "$1" &> /dev/null; then return 0; else return 1; fi;
    fi
}

# fnInstallPackage() - for fnCheckPrg() & fnProgramsFeed()
#
# INFO - Stores
#   Distributions : https://pkgs.org/ native packages (.deb & .rpm)
#   SNAP : https://snapcraft.io/ - snap packages
#   FLATPAK : https://flathub.org/ - flat packages
#   JS : https://www.npmjs.com/ - npm packages
#   PHP : https://packagist.org/ - php packages
#   PYTHON : https://pypi.org/ - pip packages
#   DOCKER : https://hub.docker.com/ - docker packages
#   RUST : https://crates.io/ - rust packages
#
#   ARGS :
#   - $1 :
#       - string, package name
#   - $2 :
#       - string, source : 'manual', 'native', 'snap', 'flat', 'npm', 'php', 'pip', 'docker', 'rust'
#   - $3 :
#       - string, mode (actually only for snap)
#   - $4 :
#       - boolean, noninteractive install
#   - $5 :
#       - string, remove repository name
#   - $6 :
#       - string, eula
#
#   RETURN : code
#
function fnInstallPackage() {
    local package="$1"
    local source="$2"
    local mode="$3"
    local noninteractive="$4"
    local removerepo="$5"
    if [[ "$6" != false || "$6" == "" ]]; then local eula="ACCEPT_EULA=Y"; else local eula="ACCEPT_EULA=N"; fi;
    local manualPath="/usr/share/fredos/sources/deb"
    # actually the most important test
    if [[ -n "$package" && -n "$source" ]]; then
        # .deb
        if command -v apt-get &> /dev/null; then
            # fnRemoverepo()
            function fnRemoverepo() {
                if [[ "$removerepo" != false && "$removerepo" != "null" ]]; then fnCheckFile "/etc/apt/sources.list.d/${removerepo}.list" true false false true; fi;
            }
            # tests : source, noninteractive & removerepo
            if [[ $source == "manual" ]]; then
                if [[ $noninteractive = true ]]; then
                    function install() { dpkg --force-confold -i "${manualPath}/${package}.deb" &> /dev/null; }
                else
                    function install() { dpkg -i "${manualPath}/${package}.deb" &> /dev/null; }
                fi
            elif [[ $source == "native" ]]; then
                # NOTE : from specific native repo
                # sourceContent="--target-release ${source} "
                if [[ $noninteractive = true ]]; then
                    function install() {
                        fnRemoverepo
                        (env DEBIAN_FRONTEND=noninteractive ${eula} apt-get install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" ${package}) &> /dev/null
                        fnRemoverepo
                        if $(fnCheckPackage "$package"); then return 0; else return 1; fi;
                    }
                 else
                    function install() {
                        fnRemoverepo
                        (env ${eula} apt-get install -y ${package}) &> /dev/null
                        fnRemoverepo
                        if $(fnCheckPackage "$package"); then return 0; else return 1; fi;
                    }
                fi
            elif [[ $source == "snap" ]]; then
                function install() {
                    if [[ "$mode" != "" && "$mode" != false && "$mode" != "null" ]]; then
                        snap install ${package} --${mode} &> /dev/null
                    else
                        snap install ${package} &> /dev/null
                    fi
                }
            elif [[ $source == "flat" ]]; then
                function install() { flatpak install flathub ${package} -y &> /dev/null; }
            elif [[ $source == "npm" ]]; then
                function install() {
                    if [[ "$mode" != "" && "$mode" != false && "$mode" != "null" ]]; then
                        npm install ${package} -${mode} &> /dev/null
                    else
                        npm install ${package} &> /dev/null
                    fi
                }
            elif [[ $source == "php" ]]; then
                function install() { composer require ${package} -y &> /dev/null; }
            elif [[ $source == "pip" ]]; then
                function install() { pip install ${package} &> /dev/null; }
            elif [[ $source == "docker" ]]; then
                function install() { docker pull ${package} &> /dev/null; }
            elif [[ $source == "rust" ]]; then
                function install() { cargo install ${package} &> /dev/null; }
            fi
            # ready for install !
            if $(install); then return 0; else return 1; fi;
        # .rpm
        elif command -v dnf &> /dev/null; then
            if dnf install -y ${package} &> /dev/null; then return 0; else return 1; fi;
        else
            fnEcho "impossible to find a package system. exit."; exit 1;
        fi
    else
        fnEcho "no package information provided. exit."; exit 1;
    fi
}

# fnCheckService() - for fnCONF()
#
#   ARGS :
#   - $1 :
#       - string service name
#
#   RETURN : code
#
function fnCheckService() {
    local service="$1";
    # SRC : https://stackoverflow.com/a/53640320/2704550
    if [[ $(systemctl list-units --all --type service --full --no-legend "$service.service" | sed 's/^\s*//g' | cut -f1 -d' ') == $service.service ]]; then
        return 0;
    else
        return 1;
    fi
}

# fnGetOS() - for fnCheckOS()
#
#   ARGS : no arg
#
#   RETURN : string or code
#
function fnGetOS() {
    if [[ $(cat /etc/os-release | grep '^NAME') == *"Debian"* ]]; then echo "debian";
    elif [[ $(cat /etc/os-release | grep '^NAME') == *"Ubuntu"* ]]; then echo "ubuntu";
    elif [[ $(cat /etc/os-release | grep '^NAME') == *"Fedora"* ]]; then echo "fedora";
    elif [[ $(hostnamectl | grep "Operating") == *"Debian"* ]]; then echo "debian";
    elif [[ $(hostnamectl | grep "Operating") == *"Ubuntu"* ]]; then echo "ubuntu";
    elif [[ $(hostnamectl | grep "Operating") == *"Fedora"* ]]; then echo "fedora";
    else return 1; fi;
}

# fnGetOSVersion() - for fnCheckOS()
#
#   ARGS : no arg
#
#   RETURN : string or code
#
function fnGetOSVersion() {
    if [[ $(cat /etc/os-release | grep '^PRETTY_NAME') == *"bookworm/sid"* ]]; then echo "12";
    elif [[ $(cat /etc/os-release | grep '^VERSION_ID') == *"11"* ]]; then echo "11";
    elif [[ $(cat /etc/os-release | grep '^VERSION_ID') == *"22.04"* ]]; then echo "22.04";
    elif [[ $(cat /etc/os-release | grep '^VERSION_ID') == *"37"* ]]; then echo "37";
    else return 1; fi;
}

# fnCheckUpdatePackage() - for multiple
#
#   ARGS : no arg
#
#   RETURN : string or code
#
function fnCheckUpdatePackage() {
    fnEcho "updating package-management system ..." "check"
    if $(fnUpdatePackage); then
        fnEcho "package-management system updated." "checked" "success"
    else
        fnEcho "impossible to update package-management system." "checked" "fail"; exit 1;
    fi
}

# fnUpdatePackage() - for fnCheckUpdatePackage()
#
#   ARGS : no arg
#
#   RETURN : code
#
function fnUpdatePackage() {
    if command -v apt-get &> /dev/null; then apt-get update &> /dev/null; return 0;
    elif command -v dnf &> /dev/null; then dnf update &> /dev/null; return 0;
    else return 1; fi;
}

# fnCheckUpgradePackage() -  for fnPROGRAMS()
#
#   ARGS : no arg
#
#   RETURN : string
#
function fnCheckUpgradePackage() {
    fnEcho "upgrade packages ..." "check"
    if $(fnGetUpgradePackage); then fnEcho "packages upgraded." "checked" "success"; else fnEcho "packages impossible to upgrade." "checked" "fail"; fi;
}

# fnGetUpgradePackage() -  for fnCheckUpgradePackage()
#
#   ARGS : no arg
#
#   RETURN : code
#
function fnGetUpgradePackage() {
    if command -v apt-get &> /dev/null; then
        # .deb
        function upgrade() {
            # natives
            DEBIAN_FRONTEND=noninteractive apt-get upgrade -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" &> /dev/null;
            DEBIAN_FRONTEND=noninteractive apt-get full-upgrade -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" &> /dev/null;
            DEBIAN_FRONTEND=noninteractive apt-get autoremove -y --purge -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" &> /dev/null;
            if command -v aptitude &> /dev/null; then
                aptitude upgrade -y &> /dev/null
            fi
            apt-get autoclean &> /dev/null
            apt-get clean &> /dev/null
            apt-get purge ~c -y &> /dev/null
            # snap & flatpak
            if command -v snap &> /dev/null; then snap refresh &> /dev/null; fi;
            if command -v flatpak &> /dev/null; then flatpak update -y &> /dev/null; fi;
        }
        if $(upgrade); then return 0; else return 1; fi;
    elif command -v dnf &> /dev/null; then
        # .rpm
        function upgrade() { dnf upgrade -y &> /dev/null; dnf clean all &> /dev/null; }
        if $(upgrade); then return 0; else return 1; fi;
    else
        return 1
    fi
}

# fnCheckMime() - for fnCreateFFCQ()
#
#   ARGS :
#   - $1 :
#       - string filename
#
#   RETURN : string
#
function fnCheckMime() { file --mime-type -b $1; }

# fnExtract() -  for fnCreateFFCQ()
#
#   ARGS :
#   - $1 :
#       - string, filename source
#   - $2 :
#       - string, filename destination
#
#   RETURN : code
#
#   SRC : https://ostechnix.com/a-bash-function-to-extract-file-archives-of-various-types/
#
function fnExtract() {
    if fnCheckFile "$1" false; then
        fnCheckDir "$2" true;
        case $1 in
            *.tar.bz2)  tar xjf "$1" -C "$2"    ;;
            *.tar.gz)   tar xzf "$1" -C "$2"    ;;
            *.tar.xz)   tar xJf "$1" -C "$2"    ;;
            *.rar)      unrar x "$1" "$2"       ;;
            *.gz)       gunzip "$1" > "$2"      ;;
            *.tar)      tar xf "$1" -C "$2"     ;;
            *.tbz2)     tar xjf "$1" -C "$2"    ;;
            *.tgz)      tar xzf "$1" -C "$2"    ;;
            *.zip)      unzip -oqq "$1" -d "$2" ;;
            *.7z)       7z x "$1" -o "$2"       ;;
            *.deb)      ar x "$1" --output "$2" ;;
            *.tar)      tar xvf "$1" -C "$2"    ;;
            *)          return 1                ;;
        esac
    else
        return 1;
    fi
}

# fnCreateFFCQ() - for fnCheckFFCQ()
#
#   ARGS :
#   - $1 :
#       - string, JSON datas array
#   - $2 :
#       - string, path
#
#   RETURN : string
#
function fnCreateFFCQ() {
    local array=$(echo $1 | jq -r ". | keys | .[]")
    # test : not empty datas array
    if [[ -n "$array" && "$array" != [] ]]; then
        for element in $array; do
            # defs independants
            local data=$(echo $1 | jq -r ".[${element}]")
            local name=$(echo $data | jq -r ".name")
            local content=$(echo $data | jq -r ".content")
            local output=$(echo $data | jq -r ".output")
            local chmod=$(echo $data | jq -r ".chmod")
            local chown=$(echo $data | jq -r ".chown")
            local chattr=$(echo $data | jq -r ".chattr")
            local status=""
            local rights=""
            local login=$(echo $data | jq -r ".login")
            # test : credentials
            if [[ -n "$login" && "$login" != "" && "$login" != "null" ]]; then
                local user_system=$(echo -e $login | cut -d ':' -f 1)
                local user_script=$(echo -e $login | cut -d ':' -f 2)
            fi
            local auth=$(echo $data | jq -r ".auth")
            # COLORS
            # FOLDER = YELLOW
            # FILE = GREEN
            # COMMAND = RED
            # QUERY = BLUE
            # test : file or folder
            if [[ "$content" == "" || "$content" == "null" ]]; then
                # defs FILE
                local path="${2}${name}"
                local raw=$(echo $data | jq -r ".raw" | sed -e "s/### by FredOS/### by FredOS ($(fnGetDate 'full'))/g")
                local src=$(echo $data | jq -r ".source")
                local extract=$(echo $data | jq -r ".extract")
                local overwrite=$(echo $data | jq -r ".overwrite")
                local replace=$(echo $data | jq -r ".replace")
                local backup=$(echo $data | jq -r ".backup")
                # defs COMMAND
                local command=$(echo $data | jq -r ".command")
                # defs QUERY
                local query=$(echo $data | jq -r ".query")
                local database=$(echo $data | jq -r ".database")
                local dependencie=$(echo $data | jq -r ".dependencie")
                local log="/srv/log/misc/sql.log"
                # test : backup
                if [[ -n "$backup" && "$backup" = true ]]; then
                    cp "$path" "$path.$(fnGetDate 'backup').backup" &> /dev/null
                fi
                # tests : RAW plain file content OR SOURCE file content OR COMMAND OR QUERY sql OR EMPTY push
                if [[ -n "$raw" && "$raw" != "" && "$raw" != "null" ]]; then
                    fnEcho " --- check RAW file ${path} ..." "check"
                    # loop data raw
                    # replace string with JSON data
                    # SRC : https://stackoverflow.com/a/49436892/2704550
                    for i in $(echo $raw | grep -Eo '\<__od.\S*__\>'); do
                        # test : password substitution from credential
                        if [[ "$i" == "__od.credential."* ]]; then
                            local i_reference=$(echo $i | sed -e "s/__//g" | sed -e "s/od.credential.//g")
                            local i_data=$(echo $varData | jq -r ".credential | [ .[] | values | select(.name==\"$i_reference\") | .password] | values[0]")
                        else
                            local i_reference=$(echo "${i}" | sed -e "s/__//g" | sed -e "s/od//g")
                            local i_data=$(echo $varData | jq -r "$i_reference")
                        fi
                        local raw=$(echo "${raw}" | sed -e "s/$i/$i_data/g")
                    done
                    # test : replace, add or push
                    if [[ -n "$replace" && "$replace" = true ]]; then
                        debug_start;
                        sed -i "$raw" "$path"  &> /dev/null
                        debug_end;
                        fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [raw, replace] checked." "checked" "success"
                    elif [[ "$overwrite" = false || "$overwrite" == "" || "$overwrite" == "null" ]]; then
                        echo "$raw" | tee -a "$path" > /dev/null
                        fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [raw, end add] checked." "checked" "success"
                    else
                        echo "$raw" | tee "$path" > /dev/null
                        fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [raw, entire push] checked." "checked" "success"
                    fi
                elif [[ -n "$src" && "$src" != "" && "$src" != "null" ]]; then
                    fnEcho " --- check SOURCE file ${path} ..." "check"
                    # test : source 'content' OR 'GIT' OR 'JSON' OR 'direct URL'
                    if [[ "$src" != "http"* && "$src" != "https"* ]]; then
                        # test : source from /pool, cp from existing source OR extract
                        if [[ "$src" == "pool"* && "$extract" != true ]]; then
                            # test : file OR directory
                            if [[ -d "${scriptPath}/fredos-datas/${src}" ]]; then
                                cp -r "${scriptPath}/fredos-datas/${src}" "$path" &> /dev/null
                            elif [[ -f "${scriptPath}/fredos-datas/${src}" ]]; then
                                cp "${scriptPath}/fredos-datas/${src}" "$path" &> /dev/null
                            fi
                            fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from pool content] checked." "checked" "success"
                        elif [[ "$src" != "pool"* && "$extract" != true ]]; then
                            # test : file OR directory
                            if [[ -d "$src" ]]; then
                                cp -r "$src" "$path" &> /dev/null
                            elif [[ -f "$src" ]]; then
                                cp "$src" "$path" &> /dev/null
                            fi
                            fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from other existing content] checked." "checked" "success"
                        elif [[ -n "$extract" && "$extract" = true ]]; then
                            # test : source from /pool OR extract from existing source
                            if [[ "$src" == "pool"* ]]; then
                                fnExtract "${scriptPath}/fredos-datas/${src}" "$path"
                                fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from pool archive] checked." "checked" "success"
                            else
                                fnExtract "$src" "$path"
                                fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from other existing archive] checked." "checked" "success"
                            fi
                        fi
                    elif [[ "$src" == *".git" ]]; then
                        fnGetDownload "git" "$src" "$path"
                        fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from GIT clone] checked." "checked" "success"
                    else
                        if fnGetDownload "wget" "$src" "$path" "$user_system"; then
                            local fileFrst=$(head -q -c 1 "$path")
                            # test : mimetype JSON
                            if [[ "$fileFrst" == "{" || "$fileFrst" == "[" ]]; then
                                local matchJson=$(echo $data | jq -r ".match")
                                local unmatchJson=$(echo $data | jq -r ".unmatch")
                                rm "$path"
                                # test : wget json
                                if fnGetDownload "wget" "$src" "${2}tmp.json" "$user_system"; then
                                    # test : support sites github & gitlab
                                    if [[ "$src" == "https://api.github.com"* ]]; then
                                        local ELEMENTS1=$(cat "${2}tmp.json" | jq ".assets | keys | .[]")
                                        # test : tarball_url or assets array
                                        if [[ "$ELEMENTS1" == "" ]]; then
                                            local urlJson=$(cat "${2}tmp.json" | jq -r ".tarball_url")
                                            fnGetDownload "wget" "$urlJson" "$path" "$user_system";
                                            fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from JSON (github wget tarball url)] checked." "checked" "success"
                                        else
                                            # loop elements
                                            for ELEMENT1 in ${ELEMENTS1}; do
                                                local githubReference=$(cat "${2}tmp.json" | jq -r ".assets[${ELEMENT1}]")
                                                local urlJson=$(echo $githubReference | jq -r ".browser_download_url")
                                                # test : un/matching extension
                                                if [[ -n "$unmatchJson" && "$unmatchJson" != "" && "$unmatchJson" != "null" ]]; then
                                                    # test : matching extension
                                                    if [[ "$urlJson" == *"$matchJson" && "$urlJson" != *"${unmatchJson}"* ]]; then
                                                        fnGetDownload "wget" "$urlJson" "$path" "$user_system"
                                                        fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from JSON (github wget release)] checked." "checked" "success"
                                                    fi
                                                elif [[ -n "$matchJson" && "$matchJson" != "" && "$matchJson" != "null" ]]; then
                                                    # test : matching extension
                                                    if [[ "$urlJson" == *"$matchJson" ]]; then
                                                        fnGetDownload "wget" "$urlJson" "$path" "$user_system"
                                                        fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from JSON (github wget release)] checked." "checked" "success"
                                                    fi
                                                else
                                                    fnGetDownload "wget" "$urlJson" "$path" "$user_system"
                                                    fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from  JSON (github wget release - default)] checked." "checked" "success"
                                                fi
                                            done
                                        fi
                                    elif [[ "$src" == "https://gitlab.com/api"* ]]; then
                                        local ELEMENTS2=$(cat "${2}tmp.json" | jq ".[0].assets.sources | keys | .[]")
                                        if [[ -n "$ELEMENTS2" ]]; then
                                            # loop elements
                                            for ELEMENT2 in ${ELEMENTS2}; do
                                                local gitlabReference=$(cat "${2}tmp.json" | jq -r ".[0].assets.sources[${ELEMENT2}]")
                                                local urlJson=$(echo $gitlabReference | jq -r ".url")
                                                # test : matching extension
                                                if [[ "$urlJson" == *"$matchJson" && "$urlJson" != *"$unmatchJson"* ]]; then
                                                    fnGetDownload "wget" "$urlJson" "$path" "$user_system"
                                                    fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from JSON (gitlab wget url - matching)] checked." "checked" "success"
                                                elif [[ "$urlJson" == *".tar.gz" && "$matchJson" == "null" && "$unmatchJson" == "null" ]]; then
                                                    fnGetDownload "wget" "$urlJson" "$path" "$user_system"
                                                    fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, from JSON (gitlab wget url - default)] checked." "checked" "success"
                                                fi
                                            done
                                        fi
                                    else
                                        fnEcho " --- file source JSON ${path} not supported." "checked" "fail"
                                    fi
                                    rm ${2}tmp.json
                                fi
                            else
                                fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [source, wget direct url] checked." "checked" "success"
                            fi
                        else
                            fnEcho " --- file source wget (test direct url) ${path} not downloaded." "checked" "fail"
                            rm ${path}
                        fi
                    fi
                elif [[ -n "$command" && "$command" != "" && "$command" != "null" ]]; then
                    fnEcho " --- check COMMAND ${path} ..." "check"
                    # loop fredos data command
                    # replace string with JSON data
                    # SRC : https://stackoverflow.com/a/49436892/2704550
                    for i in $(echo $command | grep -Eo '\<__od.\S*__\>'); do
                        # test : password substitution from credential
                        if [[ "$i" == "__od.credential."* ]]; then
                            local i_reference=$(echo $i | sed -e "s/__//g" | sed -e "s/od.credential.//g")
                            local i_data=$(echo $varData | jq -r ".credential | [ .[] | values | select(.name==\"$i_reference\") | .password] | values[0]")
                        else
                            local i_reference=$(echo "${i}" | sed -e "s/__//g" | sed -e "s/od//g")
                            local i_data=$(echo $varData | jq -r "$i_reference")
                        fi
                        local command=$(echo "${raw}" | sed -e "s/$i/$i_data/g")
                    done
                    # test : redirect output
                    if [[ -n "$output" && "$output" = true ]]; then
                        eval timeout ${CONF[TIMEOUT]} $command >>"${path}.log" 2>&1;
                        fnEcho " --- ${COLOR[RED_LIGHT]}${path}${COLOR[STOP]} [command, logged] checked." "checked" "success"
                    else
                        eval timeout ${CONF[TIMEOUT]} $command &> /dev/null;
                        fnEcho " --- ${COLOR[RED_LIGHT]}${path}${COLOR[STOP]} [command] checked." "checked" "success"
                    fi
                elif [[ -n "$query" && "$query" != "" && "$query" != "null" ]]; then
                    fnEcho " --- check QUERY ${path} ..." "check"
                    set -o noglob;
                        # loop data auth
                        # replace string with JSON data
                        # SRC : https://stackoverflow.com/a/49436892/2704550
                        for i in $(echo $auth | grep -Eo '\<__od.credential.\S*__\>'); do
                            local i_reference=$(echo $i | sed -e "s/__//g" | sed -e "s/od.credential.//g")
                            local i_data=$(echo $varData | jq -r ".credential | [ .[] | values | select(.name==\"$i_reference\") | .password] | values[0]")
                            local auth=$(echo $auth | sed -e "s/$i/$i_data/g")
                        done
                        # loop data query
                        # replace string with JSON data
                        # SRC : https://stackoverflow.com/a/49436892/2704550
                        for j in $(echo $query | grep -Eo '\<__od.credential.\S*__\>'); do
                            local j_reference=$(echo $j | sed -e "s/__//g" | sed -e "s/od.credential.//g")
                            local j_data=$(echo $varData | jq -r ".credential | [ .[] | values | select(.name==\"$j_reference\") | .password] | values[0]")
                            local query=$(echo $query | sed -e "s/$j/$j_data/g")
                        done
                        # loop data query (crypt)
                        # replace string with JSON data
                        # SRC : https://stackoverflow.com/a/49436892/2704550
                        for l in $(echo $query | grep -Eo '\<__od.crypt.\S*__\>'); do
                            local password=$(echo -e $l | sed -e "s/__//g" | sed -e "s/od.crypt.//g")
                            local l_reference=$(echo $varData | jq -r ".credential | [ .[] | values | select(.name==\"$password\") | .password] | values[0]")
                            local salt=$(echo $varData | jq -r ".credential | [ .[] | values | select(.name==\"$password\") | .salt] | values[0]")
                            debug_start;
                            local l_data=$(php -r "echo password_hash('$l_reference', PASSWORD_BCRYPT);")
                            echo -e "\n :: $l_reference | $l_data";
                            local query=$(echo $query | sed -e "s#$l#$l_data#g")
                            debug_end;
                        done
                        local truncate=${query:0:15}
                        # test : user system & user DB
                        if [[ -n "$login" && "$login" != "" && "$login" != "null" ]]; then
                            # test : need paswword
                            if [[ -n "$auth" && "$auth" != "" && "$auth" != "null" ]]; then
                                # tests : engine 'mariadb', 'mysql' OR 'psql'
                                if [[ "$name" == "mariadb" || "$name" == "mysql" ]]; then
                                    # test : db direct access
                                    if [[ -n "$database" && "$database" != "" && "$database" != "null" ]]; then
                                        function query() { sudo -u $user_system $name -u $user_script -p$auth -D $database -e "$query" >> "$log" 2>&1; }
                                    else
                                        function query() { debug_start; sudo -u $user_system $name -u $user_script -p$auth -e "$query" >> "$log" 2>&1; debug_end; }
                                    fi
                                elif [[ "$name" == "psql" ]]; then
                                    # test : db direct access
                                    if [[ -n "$database" && "$database" != "" && "$database" != "null" ]]; then
                                        function query() { sudo -u $user_system $name -U $user_script -d $database -c "$query" >> "$log" 2>&1; }
                                    else
                                        function query() { sudo -u $user_system $name -U $user_script -c "$query" >> "$log" 2>&1; }
                                    fi
                                fi
                            else
                                # tests : engine 'mariadb', 'mysql' OR 'psql'
                                if [[ "$name" == "mariadb" || "$name" == "mysql" ]]; then
                                    # test : db direct access
                                    if [[ -n "$database" && "$database" != "" && "$database" != "null" ]]; then
                                        function query() { sudo -u $user_system $name -u $user_script -D $database -e "$query" >> "$log" 2>&1; }
                                    else
                                        function query() { debug_start; sudo -u $user_system $name -u $user_script -e "$query" >> "$log" 2>&1; debug_end; }
                                    fi
                                elif [[ "$name" == "psql" ]]; then
                                    # test : db direct access
                                    if [[ -n "$database" && "$database" != "" && "$database" != "null" ]]; then
                                        function query() { sudo -u $user_system $name -U $user_script -d $database -c "$query" >> "$log" 2>&1; }
                                    else
                                        function query() { sudo -u $user_system $name -U $user_script -c "$query" >> "$log" 2>&1; }
                                    fi
                                fi
                            fi
                        else
                            fnEcho " --- user account needed." "checked" "fail"
                        fi
                        # test : supported engine
                        if [[ "$name" == "mariadb" || "$name" == "mysql" || "$name" == "psql" ]]; then
                            echo -e "QUERY : ${truncate}..." | tee -a "$log" > /dev/null;
                            # test : entire query
                            if $(query); then
                                fnEcho " --- ${COLOR[BLUE_DARK]}${truncate}... query${COLOR[STOP]} added." "checked" "success"
                            else
                                fnEcho " --- ${truncate}... query failed." "checked" "fail"
                                    # test : dependencie
                                    if [[ -n "$dependencie" && "$dependencie" = true ]]; then exit 1; fi;
                            fi
                            echo -e "\n" | tee -a "$log" > /dev/null;
                        else
                            fnEcho " --- ${name} database query engine not supported." "checked" "fail"
                        fi
                    set +o noglob;
                elif [[ "$raw" == "null" && "$src" == "null" && "$command" == "null" && "$query" == "null" ]]; then
                    fnEcho " --- check EMPTY file ${path} ..." "check"
                    fnCheckFile "$path" true
                    fnEcho " --- ${COLOR[GREEN_DARK]}${path}${COLOR[STOP]} [empty, entire push] checked." "checked" "success"
                fi
                # test : file chmod
                if [[ -n "$chmod" && "$chmod" != "" && "$chmod" != "null" ]]; then fnCheckFile "$path" false "$chmod" false false; fi;
                # test : file chown
                if [[ -n "$chown" && "$chown" != "" && "$chown" != "null" ]]; then fnCheckFile "$path" false false "$chown" false; fi;
                # test : file chattr
                if [[ -n "$chattr" && "$chattr" != "" && "$chattr" != "null" ]]; then fnCheckFile "$path" false false false "$chattr"; fi;
            else
                # defs FOLDER
                local path="${2}${name}/"
                fnEcho " - folder ${path} ..." "check"
                # test : folder exists OR not
                if fnCheckDir "$path" false; then local status="already exists"; else local status="new"; fi;
                fnCheckDir "$path" true
                # test : change folder rights
                if [[ -n "$chmod" || "$chown" != "" || "$chattr" != "null" ]]; then
                    # test : folder chmod
                    if [[ -n "$chmod" && "$chmod" != "" && "$chmod" != "null" ]]; then fnCheckDir "$path" false "$chmod" false false; local rights=" with new rights"; fi;
                    # test : folder chown
                    if [[ -n "$chown" && "$chown" != "" && "$chown" != "null" ]]; then fnCheckDir "$path" false false "$chown" false; local rights=" with new rights"; fi;
                    # test : folder chattr
                    if [[ -n "$chattr" && "$chattr" != "" && "$chattr" != "null" ]]; then fnCheckDir "$path" false false false "$chattr"; local rights=" with new rights"; fi;
                    fnEcho " - ${COLOR[YELLOW_DARK]}${path}${COLOR[STOP]} [folder, ${status}${rights}] ..." "checked" "success"
                else
                    fnEcho " - ${COLOR[YELLOW_DARK]}${path}${COLOR[STOP]} [folder, ${status}${rights}] ..." "checked" "success"
                fi
                # recall
                local data_send=$(echo $data | jq -r ".content")
                fnCreateFFCQ "${data_send}" "${path}"
            fi
        done
    fi
}

# fnCheckFFCQ() - for multiple
#
#   ARGS :
#   - $1 :
#       - string, function name at where stage action is performed
#
#   RETURN : string
#
function fnCheckFFCQ() {
    # test : at where stage action is performed
    if [[ -n "$1" && "$$1" != "" && "$$1" != "null" ]]; then
        fnEcho "find FFCQ for ${1} function ..." "check"
        local array=$(echo $varData | jq -r ".ffcq.${1} | keys | .[]")
        # test : not empty datas array
        if [[ -n $array && $array != [] ]]; then
            local data_send=$(echo $varData | jq -r ".ffcq.${1}")
            fnEcho "files for ${1} function needed." "checked" "success"
            fnCreateFFCQ "$data_send" "/"
        else
            fnEcho "files for ${1} function not needed." "checked" "success"
        fi
    else
        fnEcho "find FFCQ ..." "check"
        fnEcho "FFCQ can't be checked." "checked" "fail"
    fi
}

# fnStages() - for fnCHECKS()
#
#   ARGS : no arg
#
#   RETURN : functions
#
function fnStages() {
    function fnSOFTWARE() {
        fnNETWORK
        fnPACKSYS
        fnPROGRAMS
        fnKERNELS
        fnLANGUAGE
    }
    function fnSOFTCORE() {
        fnCRONTAB
        fnGRUB
        fnUSERS
        fnDESKTOP
        fnCONF
    }
    if [[ $(fnGetArg3) == "auto" || $(fnGetArg3) == "hardcore" ]]; then
        fnSOFTWARE
        fnSOFTCORE
        fnEND
    elif [[ $(fnGetArg3) == "software" ]]; then
        fnSOFTWARE
        fnEND
    elif [[ $(fnGetArg3) == "softcore" ]]; then
        fnSOFTCORE
        fnEND
    fi
}