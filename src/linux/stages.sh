#!/bin/bash
############################
#   ___                 ____             _
#  / _ \__   _____ _ __|  _ \  ___ _ __ | | ___  _   _ 
# | | | \ \ / / _ \ '__| | | |/ _ \ '_ \| |/ _ \| | | |
# | |_| |\ V /  __/ |  | |_| |  __/ |_) | | (_) | |_| |
#  \___/  \_/ \___|_|  |____/ \___| .__/|_|\___/ \__, |
#                                 |_|            |___/ 
############################

###########################################################################
#
# ███    ██ ███████ ████████ ██     ██  ██████  ██████  ██   ██ 
# ████   ██ ██         ██    ██     ██ ██    ██ ██   ██ ██  ██  
# ██ ██  ██ █████      ██    ██  █  ██ ██    ██ ██████  █████   
# ██  ██ ██ ██         ██    ██ ███ ██ ██    ██ ██   ██ ██  ██  
# ██   ████ ███████    ██     ███ ███   ██████  ██   ██ ██   ██ 
#
###########################################################################
function fnNETWORK() {

    ##### INTERN STAGES #####
    function fnIP() {
        fnEcho "[i] checking network configuration ..." "check";
        local fnIP="$(fnGetIP)";
        # update $varData
        varData=$(echo $varData | jq --arg fnIP "$fnIP" '.network.ip_local = $fnIP');
        local varIP=$(echo $varData | jq -r ".network.ip_local");
        # test : a valid local IP
        if [[ "$varIP" == "" || "$varIP" == "null" ]]; then
            fnEcho "[i] network system configuration failed." "checked" "fail"; exit 1;
        else
            fnEcho "[i] network system configuration checked - IP local is '${varIP}'." "checked" "success";
        fi
    }

    ##### CALLS #####
    fnEcho "NETWORK" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnIP
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    fnEcho "NETWORK" "stage" "end"
}



###########################################################################
#
# ██████   █████   ██████ ██   ██ ███████ ██    ██ ███████ 
# ██   ██ ██   ██ ██      ██  ██  ██       ██  ██  ██      
# ██████  ███████ ██      █████   ███████   ████   ███████ 
# ██      ██   ██ ██      ██  ██       ██    ██         ██ 
# ██      ██   ██  ██████ ██   ██ ███████    ██    ███████ 
#
###########################################################################
function fnPACKSYS() {

    ##### INTERN STAGES #####
    function fnKeys() {
        local gpgPath=$(echo $varData | jq -r ".gpg.configuration.path")
        fnEcho "[i] checking ${distribution^} gpg keys ..."
        {
            dirmngr < /dev/null
        } &> /dev/null
        for gpg in $(echo $varData | jq ".gpg.keys | keys | .[]"); do
            local gpgReference=$(echo $varData | jq -r ".gpg.keys[${gpg}]")
            local gpgName=$(echo $gpgReference | jq -r ".name")
            local gpgKeyserver=$(echo $gpgReference | jq -r ".s")
            local gpgFingerprint=$(echo $gpgReference | jq -r ".f")
            fnEcho "checking gpg key ${gpgName} ..." "check"
            if $(gpg --no-default-keyring --keyring "${gpgPath}/packages.${gpgName}.gpg" --keyserver ${gpgKeyserver} --recv-keys ${gpgFingerprint} &> /dev/null); then
                fnEcho "gpg key ${gpgName} (fingerprint ${gpgFingerprint}) added." "checked" "success"
            else
                fnEcho "gpg key ${gpgName} (fingerprint ${gpgFingerprint}) impossible to add." "checked" "fail"
            fi
        done
        fnEcho "[i] gpg keys checked."
    }
    function fnRepositories() {
        fnEcho "[i] checking package-management system configurations ..."
        if [[ $(fnGetOS) = "debian" || $(fnGetOS) = "ubuntu" ]]; then
            local type=$(echo $varData | jq -r ".system.type")
            local distribution=$(echo $varData | jq -r ".system.distribution")
            local release=$(echo $varData | jq -r ".system.release")
            local version=$(echo $varData | jq -r ".system.version")
            # mains repositories
            echo "#------------------------------------------------------------------------------#" | tee /etc/apt/sources.list > /dev/null
            echo "#                   ${distribution^} ${release^} ${version} REPOS OFFICIELS" | tee -a /etc/apt/sources.list > /dev/null
            echo "#------------------------------------------------------------------------------#" | tee -a /etc/apt/sources.list > /dev/null
            for main in $(echo $varData | jq -r ".pkgs.mains | keys | .[]"); do
                local repoMainReference=$(echo $varData | jq -r ".pkgs.mains[${main}]")
                local repoName=$(echo $repoMainReference | jq -r ".name")
                local repoUrl=$(echo $repoMainReference | jq -r ".u")
                local repoRelease=${release}$(echo $repoMainReference | jq -r ".r")
                local repoComponent=$(echo $repoMainReference | jq -r ".c")
                fnEcho "checking ${repoName} main repository..." "check"
                echo -e "\ndeb ${repoUrl} ${repoRelease} ${repoComponent}" | tee -a /etc/apt/sources.list > /dev/null
                echo -e "deb-src ${repoUrl} ${repoRelease} ${repoComponent}" | tee -a /etc/apt/sources.list > /dev/null
                fnEcho "repository main/${repoName} added." "checked" "success"
            done
            # extras repositories
            for extra in $(echo $varData | jq -r ".pkgs.extras | keys | .[]"); do
                local repoExtraReference=$(echo $varData | jq -r ".pkgs.extras[${extra}]")
                local repoName=$(echo $repoExtraReference | jq -r ".name")
                local repoUrl=$(echo $repoExtraReference | jq -r ".u")
                local repoRelease=$(echo $repoExtraReference | jq -r ".r")
                local repoComponent=$(echo $repoExtraReference | jq -r ".c")
                local repoArches=$(echo $repoExtraReference | jq -r ".a[]")
                local gpgPath=$(echo $varData | jq -r ".gpg.configuration.path")
                fnEcho "checking ${repoName} extra repository..." "check"
                if $(touch "/etc/apt/sources.list.d/${repoName}.list"); then
                    echo "deb [signed-by=${gpgPath}/packages.${repoName}.gpg] ${repoUrl} ${repoRelease} ${repoComponent}" | tee /etc/apt/sources.list.d/${repoName}.list > /dev/null
                    # arches fixs
                    if [[ -n $repoArches ]]; then
                        local repoArchesList=$(echo $repoExtraReference | jq --compact-output --raw-output '[ .a[] ] | values | join(",")')
                        sed -i "s/signed-by/arch=${repoArchesList} signed-by/" /etc/apt/sources.list.d/${repoName}.list
                    fi
                    fnEcho "repository extra/${repoName} added." "checked" "success"
                else
                    fnEcho "repository extra/${repoName} not added." "checked" "fail"
                fi
            done
        elif [[ $(fnGetOS) = "fedora" ]]; then
            # mains repositories
            for main in $(echo $varData | jq -r ".pkgs.mains | keys | .[]"); do
                local repoMainReference=$(echo $varData | jq -r ".pkgs.mains[${main}]")
                local repoName=$(echo $repoMainReference | jq -r ".name")
                fnEcho "checking ${repoName} main repository..." "check"
                dnf config-manager --set-enabled ${repoName}
                fnEcho "repository main/${repoName} added." "checked" "success"
            done
        fi
        # flatpaks repositories
        for flatpak in $(echo $varData | jq -r ".pkgs.flatpaks | keys | .[]"); do
            local repoFlatpakReference=$(echo $varData | jq -r ".pkgs.flatpaks[${flatpak}]")
            local repoName=$(echo $repoFlatpakReference | jq -r ".name")
            local repoUrl=$(echo $repoFlatpakReference | jq -r ".u")
            fnEcho "checking ${repoName} extra repository..." "check"
            flatpak remote-add --if-not-exists ${repoName} ${repoUrl}
            fnEcho "repository flatpak/${repoName} added." "checked" "success"
        done
        fnEcho "[i] package-management system configurations checked."
    }

    ##### CALLS #####
    fnEcho "PACKAGE MANAGEMENT SYSTEM" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnKeys
    echo "-------------------------"
    fnRepositories
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    echo "-------------------------"
    fnCheckUpdatePackage
    echo "-------------------------"
    fnCheckUpgradePackage
    fnEcho "PACKAGE MANAGEMENT SYSTEM" "stage" "end"
}



###########################################################################
#
# ██████  ██████   ██████   ██████  ██████   █████  ███    ███ ███████ 
# ██   ██ ██   ██ ██    ██ ██       ██   ██ ██   ██ ████  ████ ██      
# ██████  ██████  ██    ██ ██   ███ ██████  ███████ ██ ████ ██ ███████ 
# ██      ██   ██ ██    ██ ██    ██ ██   ██ ██   ██ ██  ██  ██      ██ 
# ██      ██   ██  ██████   ██████  ██   ██ ██   ██ ██      ██ ███████ 
#
###########################################################################
function fnPROGRAMS() {

    ##### INTERN FUNCS #####
    function fnDeleteCategories() {
        local array=("$@")
        # loop category
        for element in "${array[@]}"; do
            for category in $(echo $varData | jq ".prgs.install | keys | .[]"); do
                local categoryArray=$(echo $varData | jq -r ".prgs.install[${category}]")
                local categoryName=$(echo $categoryArray | jq -r ".name")
                if [[ "${categoryName}" == "${element}" ]]; then varData=$(echo $varData | jq "del(.prgs.install[${category}])"); fi;
            done
        done
    }
    ##### INTERN STAGES #####
    function fnProgramsThin() {
        if [[ -n "$1" && "$1" != "null" && "$1" != "" ]]; then
            local stage="$1"
            fnEcho "[i] checking : packages need to be removed ..."
            # loop packages
            for package in $(echo "$varData" | jq -r ".prgs.remove | keys | .[]"); do
                local packageArray=$(echo $varData | jq -r ".prgs.remove[${package}]")
                local packageName=$(echo $packageArray | jq -r ".name")
                local packageSource=$(echo $packageArray | jq -r ".source")
                local packageDependencie=$(echo $packageArray | jq -r ".dependencie")
                local packageStage=$(echo $packageArray | jq -r ".stage")
                if [[ "$packageStage" == "$stage" ]]; then
                    fnEcho "performing ${packageName} removal ..." "check"
                    if fnRemovePackage "${packageName}" "${packageSource}"; then
                        fnEcho "${COLOR[YELLOW_LIGHT]}${packageName}${NOCOLOR} removed." "checked" "success"
                    else
                        fnEcho "${COLOR[YELLOW_LIGHT]}${packageName}${NOCOLOR} not removed." "checked" "fail"
                        if [[ ${packageDependencie} = true ]]; then exit 1; fi;
                    fi
                fi
            done
            fnEcho "[i] packages need to be removed checked."
        else
            fnEcho "[i] stage order needed for removing packages."
        fi
    }
    function fnProgramsFeed() {
        local categoriesList="'$(echo "$varData" | jq --compact-output --raw-output '[ .prgs.install[].name ] | join("'\'', '\''")')'"
        fnEcho "[i] OS gonna be feed by auto-choices from categories ${BOLD}${categoriesList}${NOCOLOR} ..."
        fnEcho "checking : categories to delete ..." "check"
        if [[ "${varDesktopEnvironnemment}" = false ]]; then
            local array=("remarquables-gui" "fredos-gui" "gnome" "kde" "xfce")
            fnDeleteCategories "${array[@]}"
        else
            if [[ "${varDesktopEnvironnemment}" == "gnome" ]]; then
                local array=("kde" "xfce")
                fnDeleteCategories "${array[@]}"
            elif [[ "${varDesktopEnvironnemment}" == "kde" ]]; then
                local array=("gnome" "xfce")
                fnDeleteCategories "${array[@]}"
            elif [[ "${varDesktopEnvironnemment}" == "xfce" ]]; then
                local array=("gnome" "kde")
                fnDeleteCategories "${array[@]}"
            fi
        fi
        local categoriesMissList="'$(fnJoin ", " "${array[@]}")'"
        fnEcho "... but no qualified categories are ${BOLD}${categoriesMissList}${NOCOLOR} !" "checked" "success"
        # loop categories
        for category in $(echo $varData | jq ".prgs.install | keys | .[]"); do
            local categoryArray=$(echo $varData | jq -r ".prgs.install[${category}]")
            local categoryName=$(echo $categoryArray | jq -r ".name")
            local categoryDependencie=$(echo $categoryArray | jq -r ".dependencie")
            fnEcho "[i] checking : category ${COLOR[YELLOW_DARK]}${categoryName}${NOCOLOR} (#${category}) ..."
                # loop packages
                for package in $(echo $categoryArray | jq -r ".packages | keys | .[]"); do
                    local packageArray=$(echo $categoryArray | jq -r ".packages[${package}]")
                    local packageName=$(echo $packageArray | jq -r ".name")
                    local packageSource=$(echo $packageArray | jq -r ".source")
                    local packageMode=$(echo $packageArray | jq -r ".mode")
                    local packageNonInteractive=$(echo $packageArray | jq -r ".noninteractive")
                    local packageRemoveRepo=$(echo $packageArray | jq -r ".remove")
                    local packageEula=$(echo $packageArray | jq -r ".eula")
                    fnEcho "performing ${packageName} installation ..." "check"
                    if fnInstallPackage "$packageName" "$packageSource" "$packageMode" "$packageNonInteractive" "$packageRemoveRepo" "$packageEula"; then
                        fnEcho "${COLOR[YELLOW_LIGHT]}${packageName}${NOCOLOR} installed." "checked" "success"
                    else
                        fnEcho "${COLOR[YELLOW_LIGHT]}${packageName}${NOCOLOR} not installed $(if ${categoryDependencie} = true; then echo "and required. exit."; else echo "but not required. pursuit."; fi;)" "checked" "fail"
                        fnEcho "    trace : '${packageName}' '${packageSource}' '${packageMode}' '${packageNonInteractive}' '${packageRemoveRepo}' '${packageEula}'"
                        if [[ $categoryDependencie = true ]]; then exit 1; fi;
                    fi
                done
        done
    }

    ##### CALLS #####
    fnEcho "PROGRAMS" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnProgramsThin "before"
    echo "-------------------------"
    fnProgramsFeed
    echo "-------------------------"
    fnProgramsThin "after"
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    echo "-------------------------"
    fnCheckUpdatePackage
    echo "-------------------------"
    fnCheckUpgradePackage
    fnEcho "PROGRAMS" "stage" "end"
}



###########################################################################
#
# ██   ██ ███████ ██████  ███    ██ ███████ ██      ███████
# ██  ██  ██      ██   ██ ████   ██ ██      ██      ██
# █████   █████   ██████  ██ ██  ██ █████   ██      ███████
# ██  ██  ██      ██   ██ ██  ██ ██ ██      ██           ██
# ██   ██ ███████ ██   ██ ██   ████ ███████ ███████ ███████
#
###########################################################################
function fnKERNELS() {

    ##### INTERN STAGES #####
    function fnKernelsFeed() {
        local arrayKernels=$(echo $varData | jq ".kernels | keys | .[]")
        if [[ -n $arrayKernels ]]; then
            local kernelsVersions="'$(echo "$varData" | jq --compact-output --raw-output '.kernels | values | join("'\'', '\''")')'"
            fnEcho "[i] needed kernels are ${BOLD}${kernelsVersions}${NOCOLOR}.";
            # loop kernels
            for kernel in ${arrayKernels}; do
                local value=$(echo $varData | jq -r ".kernels[$kernel]");
                fnEcho "performing ${value} kernel installation ..." "check";
                if fnInstallPackage "linux-headers-${value}" "native" && fnInstallPackage "linux-headers-${value}-generic" "native" && fnInstallPackage "linux-image-${value}-generic" "native" && fnInstallPackage "linux-modules-${value}-generic" "native" && fnInstallPackage "linux-modules-extra-${value}-generic" "native"; then
                    fnEcho "${COLOR[YELLOW_LIGHT]}${value}${NOCOLOR} kernel installed." "checked" "success";
                else
                    fnEcho "${COLOR[YELLOW_LIGHT]}${value}${NOCOLOR} kernel not installed. exit." "checked" "fail"; exit 1;
                fi
            done
        else
            fnEcho "[i] no needed kernels."
        fi
    }

    ##### CALLS #####
    fnEcho "KERNELS" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnKernelsFeed
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    fnEcho "KERNELS" "stage" "end"
}



###########################################################################
#
# ██       █████  ███    ██  ██████  ██    ██  █████   ██████  ███████ 
# ██      ██   ██ ████   ██ ██       ██    ██ ██   ██ ██       ██      
# ██      ███████ ██ ██  ██ ██   ███ ██    ██ ███████ ██   ███ █████   
# ██      ██   ██ ██  ██ ██ ██    ██ ██    ██ ██   ██ ██    ██ ██      
# ███████ ██   ██ ██   ████  ██████   ██████  ██   ██  ██████  ███████
#
###########################################################################
function fnLANGUAGE() {

    ##### INTERN STAGES #####
    function fnLanguageFeed() {
        local languageData=$(echo $varData | jq ".language")
        if [[ $languageData != "null" ]]; then
            # SRC : https://serverfault.com/questions/362903/how-do-you-set-a-locale-non-interactively-on-debian-ubuntu
            fnEcho "checking language configuration ..." "check"
            if [[ $(fnGetOS) = "debian" || $(fnGetOS) = "ubuntu" ]]; then
                local languageLocale=$(echo $languageData | jq -r ".locale")
                local languageTimezone=$(echo $languageData | jq -r ".timezone")
                # date
                echo -e "$languageTimezone" > "/etc/timezone"
                ln -sf "/usr/share/zoneinfo/${languageTimezone}" "/etc/localtime" &> /dev/null
                export TZ="$languageTimezone" &> /dev/null
                dpkg-reconfigure --frontend=noninteractive tzdata &> /dev/null
                # locale
                echo -e "LANG=\"$languageLocale\"" > "/etc/default/locale"
                echo -e "$languageLocale UTF-8" > "/etc/locale.gen"
                export LANG="$languageLocale" &> /dev/null
                export LANGUAGE="$languageLocale" &> /dev/null
                export LC_ALL="$languageLocale" &> /dev/null
                locale-gen &> /dev/null
                update-locale LANG="$languageLocale" &> /dev/null
                dpkg-reconfigure --frontend=noninteractive locales &> /dev/null
            fi
            fnEcho "language configuration checked." "checked" "success"
        else
            fnEcho "[i] no needed language configuration."
        fi
    }

    ##### CALLS #####
    fnEcho "LANGUAGE" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnLanguageFeed
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    fnEcho "LANGUAGE" "stage" "end"
}



###########################################################################
#
#  ██████ ██████   ██████  ███    ██ ████████  █████  ██████  
# ██      ██   ██ ██    ██ ████   ██    ██    ██   ██ ██   ██ 
# ██      ██████  ██    ██ ██ ██  ██    ██    ███████ ██████  
# ██      ██   ██ ██    ██ ██  ██ ██    ██    ██   ██ ██   ██ 
#  ██████ ██   ██  ██████  ██   ████    ██    ██   ██ ██████ 
#
###########################################################################
function fnCRONTAB() {

    ##### INTERN STAGES #####
    function fnFeed() {
        set -o noglob;
        local array=$(echo $varData | jq ".crontab");
        # test : not empty datas array
        if [[ -n "$array" && "$array" != [] ]]; then
            # loop crontab
            for element in $(echo $array | jq "keys | .[]"); do
                local data=$(echo $array | jq -r ".[${element}]");
                local content=$(echo $data | jq -r ".content");
                fnEcho "performing #${element} crontab installation ..." "check";
                (crontab -l 2>/dev/null || true; echo $"$content";) | crontab -
                fnEcho "${COLOR[YELLOW_LIGHT]}#${element}${NOCOLOR} crontab installed." "checked" "success";
            done
        else
            fnEcho "[i] no needed crontab.";
        fi
        set +o noglob;
    }

    ##### CALLS #####
    fnEcho "CRONTAB" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnFeed
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    fnEcho "CRONTAB" "stage" "end"
}



###########################################################################
#
#  ██████  ██████  ██    ██ ██████  
# ██       ██   ██ ██    ██ ██   ██ 
# ██   ███ ██████  ██    ██ ██████  
# ██    ██ ██   ██ ██    ██ ██   ██ 
#  ██████  ██   ██  ██████  ██████
#
###########################################################################
function fnGRUB() {

    ##### INTERN STAGES #####
    function fnGRUBFeed() {
        local array=$(echo $varData | jq ".grub")
        # test : not empty datas array
        if [[ -n "$array" && "$array" != [] ]]; then
            fnEcho "[i] checking GRUB themes ..."
            # loop grub
            for element in $(echo $array | jq "keys | .[]"); do
                fnEcho "performing #${element} GRUB theme installation ..." "check";
                local data=$(echo $array | jq -r ".[${element}]")
                local name="$(echo $data | jq -r '.name')"
                local command="$(echo $data | jq -r '.command')"
                # test : command
                if [[ "$command" != "" && "$command" != "null" ]]; then eval $command &> /dev/null; fi;
                fnEcho "${COLOR[YELLOW_LIGHT]}${name}${NOCOLOR} GRUB theme installed." "checked" "success";
            done
        else
            fnEcho "[i] no needed GRUB configuration."
        fi
    }
    function fnRestarts() { if fnCheckService "gdm"; then update-grub &> /dev/null; fi; }

    ##### CALLS #####
    fnEcho "GRUB" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnGRUBFeed
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    echo "-------------------------"
    fnRestarts
    fnEcho "GRUB" "stage" "end"
}



###########################################################################
#
# ██    ██ ███████ ███████ ██████  ███████ 
# ██    ██ ██      ██      ██   ██ ██      
# ██    ██ ███████ █████   ██████  ███████ 
# ██    ██      ██ ██      ██   ██      ██ 
#  ██████  ███████ ███████ ██   ██ ███████ 
#
###########################################################################
function fnUSERS() {

    function fnCheckUser() {
        # definitions
        local username="$1";
        local fullname="$2";
        local skeleton="$3";
        local password="$4";
        local groups="$5";
        # routine
        fnEcho "checking user '$username' ('$fullname') ..." "check"
        if id "$username" &>/dev/null; then
            fnEcho "user '$username' ('$fullname') - member of '$(groups $username)', already exists." "checked" "success"
        else
            fnCreateUser "$username" "$fullname" "$skeleton" "$password" "$groups"
        fi
    }
    function fnCreateUser() {
        # definitions
        local username="$1";
        local fullname="$2";
        local skeleton="$3";
        local passwordSkeladmin=$(echo $varData | jq -r '.credential | [ .[] | values | select(.name=="skeladmin") | .password] | values[0]');
        local passwordSkeluser=$(echo $varData | jq -r '.credential | [ .[] | values | select(.name=="skeluser") | .password] | values[0]');
        # tests : passwords
        if [[ -n "$4" && "$4" != "null" ]]; then
            local password="$4";
        elif [[ "$skeleton" == "skeladmin" && -n "$passwordSkeladmin" && "$passwordSkeladmin" != "null" ]]; then
            local password="$passwordSkeladmin";
        elif [[ "$skeleton" == "skeluser" && -n "$passwordSkeluser" && "$passwordSkeluser" != "null" ]]; then
            local password="$passwordSkeluser";
        else exit 1; fi;
        local groups="$5";
        # routine
        useradd -m "$username" -c "$fullname" --create-home -k "/etc/$skeleton" --shell /bin/bash -p $(perl -e 'print crypt($ARGV[0], "password")' "$password")
        local array=$(echo $groups | jq ". | keys | .[]")
        if [[ -n $array ]]; then
            # loop
            for group in ${array}; do
                value=$(echo $groups | jq -r ".[$group]")
                usermod -a -G "$value" "$username" &>/dev/null
            done
        fi
        fnEcho "user '$username' ('$fullname') - member of '$(groups $username)', created." "checked" "success"
    }
    ##### INTERN STAGES #####
    function fnUsers() {
        local array=$(echo $varData | jq ".users.raw | keys | .[]")
        if [[ -n $array ]]; then
            fnEcho "[i] checking user management ..."
            for user in ${array}; do
                local userReference=$(echo $varData | jq -r ".users.raw[${user}]")
                local userName=$(echo $userReference | jq -r ".name")
                local userFullname=$(echo $userReference | jq -r ".fullname")
                local userSkeleton=$(echo $userReference | jq -r ".skeleton")
                local userPassword=$(echo $userReference | jq -r ".password")
                local arrayGroups=$(echo $userReference | jq -r ".groups[]")
                local userGroups=$(echo $userReference | jq -r ".groups")
                if [[ -n ${arrayGroups} ]]; then
                    local userGroupsList="additional groups '$(echo "$userReference" | jq -r -c '.groups | values | join("'\'', '\''")')'"
                else
                    local userGroupsList="no more group"
                fi
                fnEcho "[i] required checking user '$userName' ('$userFullname') - member of $userGroupsList ..."
                fnCheckUser "$userName" "$userFullname" "$userSkeleton" "$userPassword" "$userGroups"
            done
            fnEcho "[i] user management checked."
        else
            fnEcho "[i] user management checked without changes."
        fi
    }
    ##### CALLS #####
    fnEcho "USERS" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnUsers
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    fnEcho "USERS" "stage" "end"
}



###########################################################################
#
# ██████  ███████ ███████ ██   ██ ████████  ██████  ██████
# ██   ██ ██      ██      ██  ██     ██    ██    ██ ██   ██
# ██   ██ █████   ███████ █████      ██    ██    ██ ██████
# ██   ██ ██           ██ ██  ██     ██    ██    ██ ██
# ██████  ███████ ███████ ██   ██    ██     ██████  ██
#
###########################################################################
function fnDESKTOP() {

    ##### INTERN STAGES #####
    function fnExtensionsDE() {
        local desktop=$(echo $varData | jq -r ".desktop.${varDesktopEnvironnemment}")
        if [[ $varDesktopEnvironnemment = "gnome" ]]; then
            local extensionsArray=$(echo $desktop | jq -r ".extensions")
            extensionsList="'$(echo "$extensionsArray" | jq --compact-output --raw-output '[ .[] | values | select(.status=="enable") | .name] | values | join("'\'', '\''")')'"
            if [[ $extensionsArray != [] ]]; then
                fnEcho "[i] checking ${BOLD}${varDesktopEnvironnemment^}${NOCOLOR} Desktop Environnement extensions ..."
                for extension in $(echo $desktop | jq -r ".extensions | keys | .[]"); do
                    local extensionData=$(echo $extensionsArray | jq -r ".[${extension}]")
                    local extensionName=$(echo $extensionData | jq -r ".name")
                    local extensionId=$(echo $extensionData | jq -r ".id")
                    local extensionStatus=$(echo $extensionData | jq -r ".status")
                    local extensionGnome=$(echo $extensionData | jq -r ".gnome")
                    fnEcho "checking ${extensionName} ..." "check"
                    if [[ -n "$extensionGnome" && "$extensionGnome" != "null" ]]; then
                        self-gse --action=install --id=${extensionId} --gnome=${extensionGnome} --status=${extensionStatus} &> /dev/null
                    else
                        self-gse --action=install --id=${extensionId} --status=${extensionStatus} &> /dev/null
                    fi
                    if [[ -n "$extensionStatus" && "$extensionStatus" == "enable" ]]; then
                        fnEcho "${extensionName} installed & enabled." "checked" "success"
                    else
                        fnEcho "${extensionName} installed & disabled." "checked" "success"
                    fi
                done
                fnEcho "[i] extensions enabled are ${extensionsList}."
            else fnEcho "[i] no extensions for this environnement."; fi;
        else fnEcho "[i] no extensions for this environnement."; fi;
    }
    function fnThemesDE() {
        local themes=$(echo $varData | jq -r ".desktop.${varDesktopEnvironnemment}")
        local themesArray=$(echo $themes | jq -r ".themes")
        if [[ $varDesktopEnvironnemment = "gnome" ]]; then
            if [[ $themesArray != [] ]]; then
                fnEcho "[i] checking ${BOLD}${varDesktopEnvironnemment^}${NOCOLOR} Desktop Environnement themes ..."
                # loop themes
                for themesElement in $(echo $themesArray | jq "keys | .[]"); do
                    local themeData=$(echo $themesArray | jq -r ".[${themesElement}]")
                    local themeName=$(echo $themeData | jq -r ".name")
                    local themeCommand=$(echo $themeData | jq -r ".command")
                    local themeSrc=$(echo $themeData | jq -r ".source")
                    fnEcho "performing #${themesElement} theme installation ..." "check"
                    # test : themeCommand
                    if [[ "$themeCommand" != "" && "$themeCommand" != "null" ]]; then eval $themeCommand &> /dev/null; fi;
                    fnEcho "${COLOR[YELLOW_LIGHT]}${themeName}${NOCOLOR} theme installed." "checked" "success";
                done
                fnEcho "[i] ${BOLD}${varDesktopEnvironnemment^}${NOCOLOR} Desktop Environnement themes checked."
            else
                fnEcho "[i] no Desktop Environnement theme for this environnement."
            fi
        else
            fnEcho "[i] no Desktop Environnement theme needed."
        fi
    }
    function fnConfsDE() {
        fnEcho "checking misc. Desktop Environnement configurations ..." "check"
        if [[ $varDesktopEnvironnemment = "gnome" ]]; then
            for element in /home/*; do
                if fnCheckFile "$element/.config/gtk-3.0/bookmarks"; then
                    local user=$(echo "$element" | sed -e "s/\/home\///g")
                    sed -i "s/__HOMEPATH__/home\/$user/g" "${element}/.config/gtk-3.0/bookmarks" &> /dev/null
                fi
            done
        fi
        fnEcho "Misc. Desktop Environnement configurations checked." "checked" "success"
    }
    function fnCompilationsDE() {
        fnEcho "[i] compiling ${BOLD}${varDesktopEnvironnemment^}${NOCOLOR} Desktop Environnement ..." "check"
        if [[ $varDesktopEnvironnemment = "gnome" ]]; then self-gse --action=compil &> /dev/null; fi;
        fnEcho "[i] ${BOLD}${varDesktopEnvironnemment^}${NOCOLOR} Desktop Environnement compiled." "checked" "success"
    }

    ##### CALLS #####
    fnEcho "DESKTOP" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before.${varDesktopEnvironnemment}"
    echo "-------------------------"
    fnExtensionsDE
    echo "-------------------------"
    fnThemesDE
    echo "-------------------------"
    # note : compil after extension's fixs
    fnCheckFFCQ "${FUNCNAME[0]}.after.${varDesktopEnvironnemment}"
    echo "-------------------------"
    fnConfsDE
    echo "-------------------------"
    fnCompilationsDE
    fnEcho "DESKTOP" "stage" "end"
}



###########################################################################
#
#  ██████  ██████  ███    ██ ███████
# ██      ██    ██ ████   ██ ██
# ██      ██    ██ ██ ██  ██ █████
# ██      ██    ██ ██  ██ ██ ██
#  ██████  ██████  ██   ████ ██
#
###########################################################################
function fnCONF() {
    ##### INTERN STAGES #####
    function fnNginxConf() {
        fnEcho "checking Nginx configuration ..." "check"
        # test : rm default
        if fnCheckFile "/etc/nginx/sites-enabled/default" false; then rm "/etc/nginx/sites-enabled/default"; fi;
        # test : mv default
        if fnCheckFile "/etc/nginx/sites-available/default" false; then mv "/etc/nginx/sites-available/default" "/etc/nginx/sites-available/default.$(fnGetDate 'backup').backup"; fi;
        # test : create symbolic link for all .conf
        if fnCheckDir "/etc/nginx/sites-available/" false; then
            for element in /etc/nginx/sites-available/*; do
                local value=$(basename "$element")
                # test : .conf
                if [[ "$value" == *".conf" ]]; then
                    ln -s "/etc/nginx/sites-available/${value}" "/etc/nginx/sites-enabled/${value}"
                fi
                # logging
                local value_thin=$(echo "$value" | sed -e "s/.conf//g")
                sed -i "s/-__.log/-${value_thin}.log/" "/etc/nginx/sites-available/${value}"
            done
        fi
        fnEcho "Nginx configuration checked." "checked" "success"
    }

    ##### CALLS #####
    fnEcho "CONFIGURATION" "stage" "start"
    fnCheckConnect
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.before"
    echo "-------------------------"
    fnNginxConf
    echo "-------------------------"
    fnCheckFFCQ "${FUNCNAME[0]}.after"
    fnEcho "CONFIGURATION" "stage" "end"
}