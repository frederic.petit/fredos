function fnFeedADaccounts() {
    ##### OUs
    If (Test-Path $PathNewOUs) {
        $NewOUs = Import-Csv -Path $PathNewOUs -delimiter ";";
        $NewOUs | ForEach {
            $Name = $_.Name;
            $Description = $_.Description;
            $params = @{
                Name                                = "$Name";
                Description                         = "$Description";
                ProtectedFromAccidentalDeletion     = $false;
            }
            # echo
            If (Get-ADOrganizationalUnit -Filter "Name -eq '$Name'" -ErrorAction Stop) {
                Remove-ADOrganizationalUnit -Identity "OU=$Name,$GetDomainDistinguished" -Recursive -Confirm:$False;
                New-ADOrganizationalUnit @params;
                Write-Host "OU $Name updated.";
            } else {
                New-ADOrganizationalUnit @params;
                Write-Host "OU $Name created.";
            }
        }
    } else {
        Write-Host "impossible to check file ous.csv !";
    }
    ##### GROUPs
    If (Test-Path $PathNewGroups) {
        $NewGroups = Import-Csv -Path $PathNewGroups -delimiter ";";
        $NewGroups | ForEach {
            $Name = $_.Name;
            $DisplayName = " Membres de Team " + $Name;
            $SamAccountName = $_.SamAccountName;
            $params = @{
                Name            = "F1 Team " + "$Name";
                SamAccountName  = "$SamAccountName";
                Path            = "CN=Users," + "$GetDomainDistinguished";
                GroupCategory   = "Distribution";
                GroupScope      = "DomainLocal";
                DisplayName     = "$DisplayName";
                Description     = "$DisplayName";
            }
            # echo
            If (Get-ADGroup -Filter "SamAccountName -eq '$SamAccountName'" -ErrorAction Stop) {
                Remove-ADGroup -Identity "$SamAccountName" -Confirm:$False;
                New-ADGroup @params;
                Write-Host "GROUP $Name updated.";
            } else {
                New-ADGroup @params;
                Write-Host "GROUP $Name created.";
            }
        }
    } else {
        Write-Host "impossible to check file groups.csv !";
    }
    # add group security
    # src : https://www.it-connect.fr/gpo-definir-un-utilisateur-administrateur-local-de-tous-les-pcs/
    $params_security = @{
        Name            = "F1 Locale Security";
        DisplayName     = "F1 Locale Security";
        SamAccountName  = "locale-security";
        Description     = "Membres de Locale Security";
        GroupScope      = "Global";
        GroupCategory   = "Security";
        Path            = "CN=Users,$GetDomainDistinguished";
        Confirm         = $false;
    }
    # echo
    If (Get-ADGroup -Filter "SamAccountName -eq 'locale-security'" -ErrorAction Stop) {
        Remove-ADGroup -Identity "locale-security" -Confirm:$False;
        New-ADGroup @params_security;
        Write-Host "GROUP Locale Security updated.";
    } else {
        New-ADGroup @params_security;
        Write-Host "GROUP Locale Security created.";
    }
    ##### ADMINs accounts
    # add 'adminglobal' (global admin account to use on the server) & 'adminlocal' (local admin account for each computers)
    foreach ($element in $ArrayAdmins) {
        $params_admin = @{
            UserPrincipalName       = "$element@" + "$GetDomainFull";
            GivenName               = "$element";
            Surname                 = "";
            Name                    = "$element";
            SamAccountName          = "$element";
            DisplayName             = "$element";
            Initials                = "";
            ChangePasswordAtLogon   = $false;
            AccountPassword         = ConvertTo-SecureString "$GetPasswordServer" -AsPlainText -Force;
            CannotChangePassword    = $false;
            Enabled                 = $true;
            Path                    = "OU=F1-ADMINS," + "$GetDomainDistinguished";
            PasswordNeverExpires    = $false;
            AccountNotDelegated     = $true;
            ProfilePath             = "";
            Country                 = "FR";
            Confirm                 = $false;
        }
        # echo
        If (Get-ADUser -Filter "SamAccountName -eq '$element'" -ErrorAction Stop) {
            Remove-ADUser -Identity "$element" -Confirm:$False;
            New-ADUser @params_admin;
            Write-Host "USER $element@$GetDomainFull updated.";
        } else {
            New-ADUser @params_admin;
            Write-Host "USER $element@$GetDomainFull created.";
        }
        Add-ADGroupMember -Identity "locale-security" -Members "$element" -Server "$GetDomainFull"
        Add-ADGroupMember -Identity 'Administrateurs' -Members "$element" -Server "$GetDomainFull";
        Add-ADGroupMember -Identity 'Admins du domaine' -Members "$element" -Server "$GetDomainFull";
        Add-ADGroupMember -Identity 'Protected Users' -Members "$element" -Server "$GetDomainFull";
    }
    ##### USERs accounts
    If (Test-Path $PathNewUsers) {
        # note if problem for remove folder from old users : takeown /R /A /F "path" /D N + icacls "path" /grant Administrateurs:F /T /C
        # smb profiles
        If (-NOT (Test-Path 'C:\users-profiles')) {New-Item -Path 'C:\users-profiles' -itemType 'Directory' -Force | Out-Null}
        If (Get-SMBShare -Name 'users-profiles$' -ErrorAction SilentlyContinue) {
            Remove-SmbShare -Name 'users-profiles$' -Confirm:$False;
        }
        New-SmbShare -Name 'users-profiles$' -Path 'C:\users-profiles' -FullAccess "Administrateurs", "Utilisateurs authentifiés" -FolderEnumerationMode "AccessBased" -Description "users-profiles" | Out-Null;
        # smb wallpapers
        If (Get-SMBShare -Name 'users-wallpapers$' -ErrorAction SilentlyContinue) {
            Remove-SmbShare -Name 'users-wallpapers$' -Confirm:$False;
        }
        New-SmbShare -Name 'users-wallpapers$' -Path 'C:\fredos\wallpapers' -FullAccess "Administrateurs", "Utilisateurs authentifiés" -ReadAccess "Tout le monde" -CachingMode "None" -FolderEnumerationMode "AccessBased" -Description "users-wallpapers" | Out-Null;
        # smb LAPS
        If (Get-SMBShare -Name 'admins-laps$' -ErrorAction SilentlyContinue) {
            Remove-SmbShare -Name 'admins-laps$' -Confirm:$False;
        }
        New-SmbShare -Name 'admins-laps$' -Path 'C:\fredos\laps' -FullAccess "Administrateurs", "Utilisateurs authentifiés" -ReadAccess "Tout le monde" -CachingMode "None" -FolderEnumerationMode "AccessBased" -Description "admins-laps" | Out-Null;
        # accounts
        $NewUsers = Import-Csv -path $PathNewUsers -delimiter ";";
        $NewUsers | ForEach {
            $Surname = $_.Surname;
            $SurnameFst = $Surname.Substring(0,1);
            $SurnameLow = $Surname.ToLower();
            $SurnameLowFst = $SurnameFst.ToLower();
            $Name = $_.Name;
            $NameFst = $Name.Substring(0,1);
            $NameLow = $Name.ToLower();
            $NameLowFst = $NameFst.ToLower();
            $Domain = $_.Domain;
            $Unit = $_.Unit;
            $Group = $_.Group;
            # https://dovestones.com/country-codes-used-in-active-directory/
            $Country = $_.Code;
            $SamAccountName = $NameLowFst + $SurnameLow.Replace("'", "").Replace(" ", "");
            $UserPrincipalName = $SamAccountName + '@' + $Domain;
            $GivenName = $Name;
            $Initials = $NameFst + $SurnameFst;
            $NameOpe = $Name + ' ' + $Initials + '. ' + $Surname;
            $Path = "OU=$Unit," + "$GetDomainDistinguished";
            # itinerant desk
            If ($Unit -EQ "F1-TEAMS-RACERS") {
                $ProfilePath = "\\" + "$GetNameNetbios" + "\users-profiles$\%username%";
            } else {
                $ProfilePath = "";
            }
            $params = @{
                UserPrincipalName       = "$UserPrincipalName";
                GivenName               = "$GivenName";
                Surname                 = "$Surname";
                Name                    = "$NameOpe";
                SamAccountName          = "$SamAccountName";
                DisplayName             = "$NameOpe";
                Initials                = "$Initials";
                ChangePasswordAtLogon   = $false;
                AccountPassword         = ConvertTo-SecureString "$GetPasswordUser" -AsPlainText -Force;
                CannotChangePassword    = $false;
                Enabled                 = $true;
                Path                    = "$Path";
                PasswordNeverExpires    = $false;
                ProfilePath             = "$ProfilePath";
                Country                 = "$Country";
                Confirm                 = $false;
            }
            # echo
            If (Get-ADUser -Filter "SamAccountName -eq '$SamAccountName'" -ErrorAction Stop) {
                Remove-ADUser -Identity "$SamAccountName" -Confirm:$False;
                Write-Host "USER $Name $Surname/$UserPrincipalName updated.";
            } else {
                Write-Host "USER $Name $Surname/$UserPrincipalName created.";
            }
            New-ADUser @params;
            If ($Group -NE "x") {
                Add-ADGroupMember -Identity "$Group" -Members "$SamAccountName" -Server "$GetDomainFull"
            }
        }
    } else {
        Write-Host "impossible to check file users.csv !";
    }
}


function fnFeedADgpos() {
    ##### GPOs
    # GPO refresh
    If (Get-GPO -Name "GPO_Refresh" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_Refresh" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_Refresh = New-GPO -Name "GPO_Refresh" -Domain "$GetDomainFull";
    $GPO_Refresh | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\System\' -ValueName 'GroupPolicyRefreshTime' -Value "30" -Type 'STRING' -Action 'Create';
    $GPO_Refresh | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\System\' -ValueName 'GroupPolicyRefreshTime' -Value "30" -Type 'STRING' -Action 'Update';
    New-GPLink -Name "GPO_Refresh" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Refresh" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Refresh" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO regedit
    If (Get-GPO -Name "GPO_DisableReg" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_DisableReg" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_DisableReg = New-GPO -Name "GPO_DisableReg" -Domain "$GetDomainFull";
    $GPO_DisableReg | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName "DisableRegistryTools" -Value 1 -Type 'DWORD' -Action 'Create';
    $GPO_DisableReg | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName "DisableRegistryTools" -Value 1 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_DisableReg" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    # GPO cmd
    If (Get-GPO -Name "GPO_DisableCMD" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_DisableCMD" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_DisableCMD = New-GPO -Name "GPO_DisableCMD" -Domain "$GetDomainFull";
    $GPO_DisableCMD | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Policies\Microsoft\Windows\System\' -ValueName "DisableCMD" -Value 1 -Type 'DWORD' -Action 'Create';
    $GPO_DisableCMD | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Policies\Microsoft\Windows\System\' -ValueName "DisableCMD" -Value 1 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_DisableCMD" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    # GPO enumerate
    If (Get-GPO -Name "GPO_Enumerate" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_Enumerate" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_Enumerate = New-GPO -Name "GPO_Enumerate" -Domain "$GetDomainFull";
    $GPO_Enumerate | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\System\' -ValueName "EnumerateLocalUsers" -Value 1 -Type 'DWORD' -Action 'Create';
    $GPO_Enumerate | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\System\' -ValueName "EnumerateLocalUsers" -Value 1 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_Enumerate" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Enumerate" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Enumerate" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO wallpaper
    If (Get-GPO -Name "GPO_Wallpaper" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_Wallpaper" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_Wallpaper = New-GPO -Name "GPO_Wallpaper" -Domain "$GetDomainFull";
    $GPO_Wallpaper | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\ActiveDesktop\' -ValueName 'NoChangingWallPaper' -Value 0 -Type 'DWORD' -Action 'Create';
    $GPO_Wallpaper | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\ActiveDesktop\' -ValueName 'NoChangingWallPaper' -Value 0 -Type 'DWORD' -Action 'Update';
    $GPO_Wallpaper | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName 'Wallpaper' -Value "\\$GetNameNetbios\users-wallpapers$\formule1.jpg" -Type 'STRING' -Action 'Create';
    $GPO_Wallpaper | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName 'WallpaperStyle' -Value "6" -Type 'STRING' -Action 'Create';
    $GPO_Wallpaper | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName 'Wallpaper' -Value "\\$GetNameNetbios\users-wallpapers$\formule1.jpg" -Type 'STRING' -Action 'Update';
    $GPO_Wallpaper | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName 'WallpaperStyle' -Value "6" -Type 'STRING' -Action 'Update';
    New-GPLink -Name "GPO_Wallpaper" -Target "OU=F1-ADMINS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Wallpaper" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Wallpaper" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Wallpaper" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO show files extensions
    If (Get-GPO -Name "GPO_FilesExtensions" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_FilesExtensions" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_FilesExtensions = New-GPO -Name "GPO_FilesExtensions" -Domain "$GetDomainFull";
    $GPO_FilesExtensions | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\' -ValueName "HideFileExt" -Value 0 -Type 'DWORD' -Action 'Create';
    $GPO_FilesExtensions | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\' -ValueName "HideFileExt" -Value 0 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_FilesExtensions" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_FilesExtensions" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_FilesExtensions" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO show hidden files
    If (Get-GPO -Name "GPO_HiddenFiles" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_HiddenFiles" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_HiddenFiles = New-GPO -Name "GPO_HiddenFiles" -Domain "$GetDomainFull";
    $GPO_HiddenFiles | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\' -ValueName "Hidden" -Value 1 -Type 'DWORD' -Action 'Create';
    $GPO_HiddenFiles | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\' -ValueName "Hidden" -Value 1 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_HiddenFiles" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_HiddenFiles" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_HiddenFiles" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO left taskbar
    If (Get-GPO -Name "GPO_LeftTaskbar" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_LeftTaskbar" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_LeftTaskbar = New-GPO -Name "GPO_LeftTaskbar" -Domain "$GetDomainFull";
    $GPO_LeftTaskbar | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\' -ValueName "TaskbarAl" -Value 0 -Type 'DWORD' -Action 'Create';
    $GPO_LeftTaskbar | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\' -ValueName "TaskbarAl" -Value 0 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_LeftTaskbar" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_LeftTaskbar" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_LeftTaskbar" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO old contextual menu
    If (Get-GPO -Name "GPO_ContextualMenu" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_ContextualMenu" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_ContextualMenu = New-GPO -Name "GPO_ContextualMenu" -Domain "$GetDomainFull";
    $GPO_ContextualMenu | Set-GPPrefRegistryValue -Context 'User' -Key "HKEY_CURRENT_USER\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32\" -Action 'Create';
    $GPO_ContextualMenu | Set-GPPrefRegistryValue -Context 'User' -Key "HKEY_CURRENT_USER\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32\" -Action 'Update';
    New-GPLink -Name "GPO_ContextualMenu" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_ContextualMenu" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_ContextualMenu" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO old explorer ribbon
    If (Get-GPO -Name "GPO_ExplorerRibbon" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_ExplorerRibbon" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_ExplorerRibbon = New-GPO -Name "GPO_ExplorerRibbon" -Domain "$GetDomainFull";
    $GPO_ExplorerRibbon | Set-GPPrefRegistryValue -Context 'User' -Key "HKEY_CURRENT_USER\Software\Classes\CLSID\{d93ed569-3b3e-4bff-8355-3c44f6a52bb5}\InprocServer32\" -Action 'Create';
    $GPO_ExplorerRibbon | Set-GPPrefRegistryValue -Context 'User' -Key "HKEY_CURRENT_USER\Software\Classes\CLSID\{d93ed569-3b3e-4bff-8355-3c44f6a52bb5}\InprocServer32\" -Action 'Update';
    New-GPLink -Name "GPO_ExplorerRibbon" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_ExplorerRibbon" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_ExplorerRibbon" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO disable weather widget
    If (Get-GPO -Name "GPO_WeatherWidget" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_WeatherWidget" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_WeatherWidget = New-GPO -Name "GPO_WeatherWidget" -Domain "$GetDomainFull";
    $GPO_WeatherWidget | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Dsh\' -ValueName "AllowNewsAndInterests" -Value 0 -Type 'DWORD' -Action 'Create';
    $GPO_WeatherWidget | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Dsh\' -ValueName "AllowNewsAndInterests" -Value 0 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_WeatherWidget" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_WeatherWidget" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_WeatherWidget" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO disable cortana
    If (Get-GPO -Name "GPO_Cortana" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_Cortana" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_Cortana = New-GPO -Name 'GPO_Cortana' -Domain "$GetDomainFull";
    $GPO_Cortana | Set-GPPrefRegistryValue -Context 'Computer' -Key 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsSearch\' -ValueName 'AllowCortana' -Value 0 -Type 'DWORD' -Action 'Create';
    $GPO_Cortana | Set-GPPrefRegistryValue -Context 'Computer' -Key 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsSearch\' -ValueName 'AllowCortana' -Value 0 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_Cortana" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Cortana" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Cortana" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO verbose
    If (Get-GPO -Name "GPO_Verbose" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_Verbose" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_Verbose = New-GPO -Name 'GPO_Verbose' -Domain "$GetDomainFull";
    $GPO_Verbose | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName 'VerboseStatus' -Value 1 -Type 'DWORD' -Action 'Create';
    $GPO_Verbose | Set-GPPrefRegistryValue -Context 'User' -Key 'HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\' -ValueName 'VerboseStatus' -Value 1 -Type 'DWORD' -Action 'Update';
    New-GPLink -Name "GPO_Verbose" -Target "OU=F1-BOARD,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Verbose" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished";
    New-GPLink -Name "GPO_Verbose" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished";
    # GPO security
    If (Get-GPO -Name "GPO_LocalSec" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_LocalSec" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_LocalSec = New-GPO -Name "GPO_LocalSec" -Domain "$GetDomainFull";
    New-GPLink -Name "GPO_LocalSec" -Target "OU=F1-ADMINS,$GetDomainDistinguished";
}