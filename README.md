# FredOS
**GNU/Linux cuztomisations** for distributions Debian 11/12, Ubuntu 22.04 & Fedora 37 with GNOME/KDE/XFCE.

Goals here are to provide a system ready for learn IT support and some basics development tools.

Datas are provided from JSON files with desktop or server profiles (not available here).

Get the script, launch it and wait for the magic !

This is a Cloud-Init inspiration but can working with her, Syslinux or on current installation, in **bash**.

![FredOS interface](https://gitlab.com/fredericpetit/fredos/-/raw/main/interface.png)

## FredUnit membership
**This project is a part of FredUnit, see installations recommandations, live-demo, thanks and credit here : https://fredericpetit.fr/unit.html.**