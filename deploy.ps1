############################
#   ______            _    ____   _____ 
#  |  ____|          | |  / __ \ / ____|
#  | |__ _ __ ___  __| | | |  | | (___  
#  |  __| '__/ _ \/ _' | | |  | |\___ \ 
#  | |  | | |  __/ (_| | | |__| |____) |
#  |_|  |_|  \___|\__,_|  \____/|_____/ 
#
# by : fredericpetit.fr
############################
# script: deploy.ps1
# usage : main script
# standalone download : Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/fredericpetit/fredos/-/raw/main/deploy.ps1'))
############################

##### rights
#Set-ExecutionPolicy -Scope "CurrentUser" -ExecutionPolicy "Unrestricted" -Confirm:$false -Force

##### variables
$GetScriptSrc = "https://gitlab.com/fredericpetit/fredos/-/raw/main";
$GetDate = Get-Date;
$GetNetwork = "1";
$GetIp = "192.168.1.2";
$GetIpDNS = "192.168.1.1";
$GetIpSubnet1 = "192.168.1.0/24";
$GetIpSubnet2 = "172.16.26.0/24";
$GetDomaineName = "global";
$GetDomaineNameNetbios = $GetDomaineName.ToUpper();
$GetDomainTLD = "intra";
$GetDomainFull = "$GetDomaineName.$GetDomainTLD";
$GetDomainDistinguished = "DC=$GetDomaineName,DC=$GetDomainTLD";
$GetPasswordServer = "formule1!formule1!";
$GetPasswordUser = "Formule1!Formule1!";
$GetPathMain = "C:\fredos";
$PathNewOUs = "$GetPathMain" + "\datas\ous.csv";
$PathNewGroups = "$GetPathMain" + "\datas\groups.csv";
$PathNewUsers = "$GetPathMain" + "\datas\users.csv";
if ((Get-CimInstance Win32_OperatingSystem).ProductType -gt 1) {
    $GetOS = "server";
    $GetName = "srv-" + "$GetDomaineName";
    $GetNameNetbios = "SRV-" + "$GetDomaineNameNetbios";
} else {
    $GetOS = "desktop";
    $GetName = "clt-" + "$GetDomaineName" + "-win1";
    $GetNameNetbios = "CLT-" + "$GetDomaineNameNetbios";
}
$ArrayAdmins = @("adminglobal", "adminlocal");

##### fredos softwares, paths, regedit & shortcuts
$ArraySoftware1 = @("vcredist-all", "dotnet", "dotnet-runtime", "dotnet-6.0-runtime", "git", "jq", "chocolateygui", "sysinternals", "firefox", "7zip", "vscode", "pwsh", "regscanner");
$ArraySoftware2 = @("libreoffice-fresh", "gimp", "adobereader", "googlechrome", "thunderbird", "vlc", "slack", "discord", "teamviewer", "obs-studio", "ccleaner", "putty", "postman");
$ArrayPaths = @("fredos", "fredos\audit", "fredos\backup", "fredos\configs", "fredos\datas", "fredos\git", "fredos\laps", "fredos\logs","fredos\sources", "fredos\tmp", "fredos\wallpapers");
foreach ($element in $ArrayPaths) {
    If (-NOT (Test-Path "C:\$element")) {New-Item -Path "C:\$element" -itemType 'Directory' -Force | Out-Null}
}
If (-NOT (Test-Path 'HKU:\')) {New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS | Out-Null}
If (-NOT (Test-Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced')) {New-Item -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Force | Out-Null}
if ($GetOS -eq "server") {
    $shortcut1Path = "C:\Users\Administrateur\Desktop\FredOS.lnk";
    If (-NOT (Test-Path "$shortcut1Path")) {
        $shortcut1Source = "C:\fredos\";
        $shortcut1Obj = New-Object -ComObject ("WScript.Shell");
        $shortcut1 = $shortcut1Obj.CreateShortcut($shortcut1Path);
        $shortcut1.TargetPath = $shortcut1Source;
        $shortcut1.WindowStyle = 1;
        $shortcut1.Save();
    }
}

##### fredos includes
Invoke-WebRequest -Uri "${GetScriptSrc}/src/windows/feedAD.ps1" -OutFile "C:\fredos\tmp\feedAD.ps1";
. "C:\fredos\tmp\feedAD.ps1";
Invoke-WebRequest -Uri "${GetScriptSrc}/src/windows/pingcastle.ps1" -OutFile "C:\fredos\tmp\pingcastle.ps1";
. "C:\fredos\tmp\pingcastle.ps1";

##### choices
function fnBasics1() {
    # language
    Set-Culture -CultureInfo fr-FR
    Set-WinSystemLocale -SystemLocale fr-FR
    Set-WinUILanguageOverride -Language fr-FR
    Set-WinUserLanguageList fr-FR -Force
    # time (NTP)
    Set-ItemProperty -Path 'HKLM:\SOFTWAREM\icrosoft\Windows\CurrentVersion\DateTime\Servers' -Name '1' -Value 'europe.pool.ntp.org' -Force
    New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers' -Name '1' -Value 'europe.pool.ntp.org' -PropertyType String -Force
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers' -Name '2' -Value 'time.windows.com' -Force
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DateTime\Servers' -Name '2' -Value 'time.windows.com' -Force
    net start w32time
    w32tm /resync
    # add NuGet pack
    Install-PackageProvider -Name "NuGet" -Force -Verbose -Confirm:$false
    Import-PackageProvider -Name "NuGet" -Force -Verbose
    Register-PackageSource -Name "NuGet" -Location https://www.nuget.org/api/v2 -ProviderName "NuGet" -Force -Verbose -Confirm:$false
    # updates WU
    Install-Module -Name 'PSWindowsUpdate' -Force -Verbose
    Import-Module PSWindowsUpdate
    Install-WindowsUpdate -AcceptAll -Install | Out-File "c:\fredos\logs\$(get-date -f yyyy-MM-dd)-WindowsUpdate.log" -Force
    # updates Store (desktop only)
    if ($GetOS -eq "desktop") {
        Get-CimInstance -Namespace "Root\cimv2\mdm\dmmap" -ClassName "MDM_EnterpriseModernAppManagement_AppManagement01" | Invoke-CimMethod -MethodName UpdateScanMethod
    }
    # add Chocolatey pack
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    choco upgrade all -y --ignore-checksums
    foreach ($software in $ArraySoftware1) {choco install -y --ignore-checksums "$software";}
    if ($GetOS -eq "server") {choco install -y --ignore-checksums laps --params='"/ALL"';}
}
function fnBasics2() {
    # downloads git
    If (-NOT (Test-Path 'C:\fredos\git\fredos')) {
        New-Item -Path 'C:\fredos\git\fredos' -itemType 'Directory' -Force | Out-Null
        git clone https://gitlab.com/fredericpetit/fredos.git "C:\fredos\git\fredos"
    } else {
        git -C "C:\fredos\git\fredos" pull
    }
    # downloads individuals (server 1st, desktop 2nd)
    # src : https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-webrequest
    if ($GetOS -eq "server") {
        $files = @(
            @{
                Uri = "https://github.com/massgravel/Microsoft-Activation-Scripts/releases/download/1.6/MAS_1.6_Password_1234.7z"
                OutFile = "$GetPathMain" + "\sources\mas.7z"
            },
            @{
                Uri = "https://github.com/vletoux/pingcastle/releases/download/2.11.0.1/PingCastle_2.11.0.1.zip"
                OutFile = "$GetPathMain" + "\sources\pingcastle.zip"
            },
            @{
                Uri = "https://www.glenn.delahoy.com/downloads/desktopinfo/DesktopInfo3080.zip"
                OutFile = "$GetPathMain" + "\sources\desktopinfo.zip"
            },
            @{
                Uri = "https://gitlab.com/fredericpetit/fredos-datas/-/raw/main/datas/windows/groups.csv"
                OutFile = "$GetPathMain" + "\datas\groups.csv"
            },
            @{
                Uri = "https://gitlab.com/fredericpetit/fredos-datas/-/raw/main/datas/windows/ous.csv"
                OutFile = "$GetPathMain" + "\datas\ous.csv"
            },
            @{
                Uri = "https://gitlab.com/fredericpetit/fredos-datas/-/raw/main/datas/windows/users.csv"
                OutFile = "$GetPathMain" + "\datas\users.csv"
            }
        )
    } else {
        $files = @(
            @{
                Uri = "https://github.com/massgravel/Microsoft-Activation-Scripts/releases/download/1.6/MAS_1.6_Password_1234.7z"
                OutFile = "$GetPathMain" + "\sources\mas.7z"
            },
            @{
                Uri = "https://www.glenn.delahoy.com/downloads/desktopinfo/DesktopInfo3080.zip"
                OutFile = "$GetPathMain" + "\sources\desktopinfo.zip"
            }
        )
    }
    $jobs = @()
    foreach ($file in $files) {
        $jobs += Start-ThreadJob -Name $file.OutFile -ScriptBlock {
            $params = $using:file
            Invoke-WebRequest @params
        }
    }
    Write-Host "Downloads started ..."
    Wait-Job -Job $jobs
    foreach ($job in $jobs) {
        Receive-Job -Job $job
    }
    Write-Host "Downloads finished."
    & "C:\Program Files\7-Zip\7z.exe" x -y "C:\fredos\sources\mas.7z" -o"C:\Program Files\MAS\" -p1234
    & "C:\Program Files\7-Zip\7z.exe" x -y "C:\fredos\sources\desktopinfo.zip" -o"C:\Program Files\DesktopInfo\"
    & "C:\Program Files\DesktopInfo\DesktopInfo64.exe"
    if ($GetOS -eq "server") {
        & "C:\Program Files\7-Zip\7z.exe" x -y "C:\fredos\sources\pingcastle.zip" -o"C:\Program Files\PingCastle\"
    }
    # autostart DesktopInfo
    New-Itemproperty -path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Run' -Name 'DesktopInfo' -value '"C:\Program Files\DesktopInfo\DesktopInfo64.exe"' -PropertyType String -Force
    # no CTRL/ALT
    Set-Itemproperty -path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -Name 'DisableCAD' -value '1'
    New-Itemproperty -path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -Name 'DisableCAD' -value '1' -PropertyType DWORD -Force
    # show files extensions
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'HideFileExt' -Value '0' -Force
    New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'HideFileExt' -Value '0' -PropertyType DWORD -Force
    # show hidden files
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'Hidden' -Value '1' -Force
    New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'Hidden' -Value '1' -PropertyType DWORD -Force
    # left taskbar
    Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'TaskbarAl' -Value '0' -Force
    New-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced' -Name 'TaskbarAl' -Value '0' -PropertyType DWORD -Force
    # old contextual menu
    New-Item -Path 'HKCU:\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}' -Force
    New-Item -Path 'HKCU:\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32' -Value '' -Force
    # old explorer ribbon
    New-Item -Path 'HKCU:\Software\Classes\CLSID\{d93ed569-3b3e-4bff-8355-3c44f6a52bb5}' -Force
    New-Item -Path 'HKCU:\Software\Classes\CLSID\{d93ed569-3b3e-4bff-8355-3c44f6a52bb5}\InprocServer32' -Value '' -Force
    # disable weather widget
    New-Item -Path 'HKLM:\Software\Policies\Microsoft\Dsh' -Force
    New-ItemProperty -Path 'HKLM:\Software\Policies\Microsoft\Dsh\' -Name 'AllowNewsAndInterests' -Value '0' -PropertyType DWORD -Force
    # disable mapsbroker & presse-papier (to-do)
    Get-Service -Name 'MapsBroker' | Set-Service -StartupType 'Disabled' -Confirm:$False
    #Get-Service -Name 'cbdhsvc*' | Set-Service -StartupType 'Disabled' -Confirm:$False
    # verr num
    Set-ItemProperty -Path 'HKU:\.DEFAULT\Control Panel\Keyboard' -Name 'InitialKeyboardIndicators' -Value '2' -Force
    New-ItemProperty -Path 'HKU:\.DEFAULT\Control Panel\Keyboard' -Name 'InitialKeyboardIndicators' -Value '2' -PropertyType String -Force
    Set-ItemProperty -Path 'HKCU:\Control Panel\Keyboard' -Name 'InitialKeyboardIndicators' -Value '2' -Force
    New-ItemProperty -Path 'HKCU:\Control Panel\Keyboard' -Name 'InitialKeyboardIndicators' -Value '2' -PropertyType String -Force
    # screensaver timeout
    Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name 'ScreenSaveTimeOut' -Value '1800' -Force
    New-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name 'ScreenSaveTimeOut' -Value '1800' -PropertyType String -Force
    # FredOS wallpaper
    If ($GetOS -eq "server") {
        Copy-Item 'C:\fredos\git\fredos\files\fredericpetitfr\wallpaper.png' -Destination 'C:\fredos\wallpapers\main.png';
        Copy-Item 'C:\fredos\git\fredos\files\ldap\formule1.jpg' -Destination 'C:\fredos\wallpapers\formule1.jpg';
        Set-Itemproperty -Path 'HKCU:\Control Panel\Desktop' -Name 'WallPaper' -Value 'C:\fredos\wallpapers\main.png' -Force;
    }
    # LAPS
    If ($GetOS -eq "server") {
        Copy-Item 'C:\fredos\git\fredos\files/laps/LAPS.x64.msi' -Destination 'C:\fredos\laps\LAPS.x64.msi';
        Copy-Item 'C:\fredos\git\fredos\files/laps/LAPS.x86.msi' -Destination 'C:\fredos\laps\LAPS.x86.msi';
    }
    # git cleanup
    If ($GetOS -eq "desktop") {
        If (Test-Path 'C:\fredos\git\fredos') {
            Remove-Item -Path 'C:\fredos\git\fredos' -Recurse -Force;
        }
    }
    # restart explorer service
    Stop-Process -processName: Explorer -Force;
}
function fnRename() {
    irm https://massgrave.dev/get | iex;
    Rename-Computer -NewName "$GetName";
}
function fnConfigNetwork() {
    # rename
    If (Get-NetAdapter -IncludeHidden | where Name -eq 'Ethernet0') {Rename-NetAdapter -Name 'Ethernet0' -NewName 'WAN' -Confirm:$False;}
    If (Get-NetAdapter -IncludeHidden | where Name -eq 'Ethernet1') {Rename-NetAdapter -Name 'Ethernet1' -NewName 'LAN' -Confirm:$False;}
    # wan card
    If ($GetNetwork -eq "2") {
        Disable-NetAdapterBinding -InterfaceAlias 'WAN' -ComponentID 'ms_tcpip6' -Confirm:$False;
        Restart-NetAdapter -Name 'WAN' -Confirm:$False;
        Get-NetIPConfiguration -InterfaceAlias 'WAN' | Out-File -FilePath C:\fredos\configs\wan.txt -Confirm:$False;
    } else {
        Disable-NetAdapter -Name "WAN" -Confirm:$false;
    }
    # lan card
    Disable-NetAdapterBinding -InterfaceAlias 'LAN' -ComponentID 'ms_tcpip6' -Confirm:$False;
    Remove-NetIPAddress -InterfaceAlias 'LAN' -Confirm:$False;
    Remove-NetRoute -InterfaceAlias 'LAN' -Confirm:$False;
    New-NetIPAddress -InterfaceAlias 'LAN' -IPAddress "$GetIp" -PrefixLength 24 -DefaultGateway "$GetIpDNS" -Confirm:$False | Out-Null;
    Set-DnsClientServerAddress -InterfaceAlias 'LAN' -ServerAddresses ("$GetIpDNS") -Confirm:$False | Out-Null;
    Restart-NetAdapter -Name 'LAN' -Confirm:$False;
    Get-NetIPConfiguration -InterfaceAlias 'LAN' | Out-File -FilePath C:\fredos\configs\lan.txt -Confirm:$False;
}
function fnConfigAD() {
    # ad-ds + dns
    $FeatureList = @("AD-Domain-Services","DNS","Windows-Server-Backup");
    Foreach($Feature in $FeatureList) {
        if (((Get-WindowsFeature -Name $Feature).InstallState) -eq "Available") {
            Write-Output "La fonctionnalité $Feature va être mise en place !";
            Try {
                Add-WindowsFeature -Name $Feature -IncludeManagementTools -IncludeAllSubFeature -Verbose;
                Write-Output "$Feature : Installation terminée avec succès !";
            } Catch {
                Write-Output "$Feature : Erreur à l'installation !";
            }
        }
    }
    $ForestConfiguration = @{
        '-DatabasePath'                     = 'C:\Windows\NTDS';
        '-DomainMode'                       = 'Default';
        '-DomainName'                       = "$GetDomainFull";
        '-DomainNetbiosName'                = "$GetDomaineNameNetbios";
        '-ForestMode'                       = 'Default';
        '-InstallDns'                       = $true;
        '-LogPath'                          = 'C:\Windows\NTDS';
        '-NoRebootOnCompletion'             = $false;
        '-SysvolPath'                       = 'C:\Windows\SYSVOL';
        '-SafeModeAdministratorPassword'    = ConvertTo-SecureString "$GetPasswordServer" -AsPlaintext -Force;
        '-Force'                            = $true;
        '-CreateDnsDelegation'              = $false
    }
    Import-Module ADDSDeployment
    Install-ADDSForest @ForestConfiguration
    # if forest already exists
    #Install-ADDSDomainController -DomainName $GetDomainFull -InstallDns:$true -Credential (Get-Credential "GLOBAL\Administratreur")
}
function fnConfigDHCP() {
    # dhcp
    $FeatureList = @("DHCP");
    Foreach($Feature in $FeatureList) {
        if (((Get-WindowsFeature -Name $Feature).InstallState) -eq "Available") {
            Write-Output "La fonctionnalité $Feature va être mise en place !";
            Try {
                Add-WindowsFeature -Name $Feature -IncludeManagementTools -IncludeAllSubFeature -Verbose;
                Write-Output "$Feature : Installation terminée avec succès !";
            } Catch {
                Write-Output "$Feature : Erreur à l'installation !";
            }
        }
    }
    Add-DhcpServerv4Scope -Name "etendue01" -StartRange 192.168.1.50 -EndRange 192.168.1.100 -SubnetMask 255.255.255.0
    Set-DhcpServerv4OptionDefinition -OptionId 3 -DefaultValue 192.168.1.11
    Set-DhcpServerv4OptionDefinition -OptionId 15 -DefaultValue "$GetDomainFull"
    Set-DhcpServerv4Scope -ScopeId 192.168.1.0 -Name "etendue01" -State Active
}
function fnConfigRouting() {
    # routing
    $FeatureList = @("Routing");
    Foreach($Feature in $FeatureList) {
        if (((Get-WindowsFeature -Name $Feature).InstallState) -eq "Available") {
            Write-Output "La fonctionnalité $Feature va être mise en place !";
            Try {
                Add-WindowsFeature -Name $Feature -IncludeManagementTools -IncludeAllSubFeature -Verbose;
                Write-Output "$Feature : Installation terminée avec succès !";
            } Catch {
                Write-Output "$Feature : Erreur à l'installation !";
            }
        }
    }
}
function fnConfigIIS() {
    # IIS
    $FeatureList = @("Web-Common-Http", "Web-Health", "Web-Security", "Web-Ftp-Server", "Web-Mgmt-Console", "NET-HTTP-Activation");
    Foreach($Feature in $FeatureList) {
        if (((Get-WindowsFeature -Name $Feature).InstallState) -eq "Available") {
            Write-Output "La fonctionnalité $Feature va être mise en place !";
            Try {
                Add-WindowsFeature -Name $Feature -IncludeManagementTools -IncludeAllSubFeature -Verbose;
                Write-Output "$Feature : Installation terminée avec succès !";
            } Catch {
                Write-Output "$Feature : Erreur à l'installation !";
            }
        }
    }
}
function fnInstallMore() {
    foreach ($software in $ArraySoftware2) {choco install -y --ignore-checksums "$software";}
    Set-Itemproperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Run' -Name 'Discord' -Value '';
}
function fnJoinDomain() {
    Add-Computer -DomainName "$GetDomainFull" -Credential "$GetDomaineNameNetbios\adminlocal" -Restart -Force;
}
function fnADMX() {
    ##### ADMX
    # src : https://www.it-connect.fr/chapitres/gpo-magasin-central-et-fichiers-adm-admx-et-adml/
    if ($GetOS -eq "server") {
        If (-NOT (Test-Path "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions")) {
            Write-Host "admx build needed ..." -ForegroundColor "blue";
            New-Item -Path "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies" -itemType 'Directory' -Force | Out-Null;
            New-Item -Path "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions" -itemType 'Directory' -Force | Out-Null;
            Invoke-WebRequest -Uri "${GetScriptSrc}/files/admx/admx.msi" -OutFile "C:\fredos\tmp\admx.msi";
            Start-Process msiexec.exe -Wait -ArgumentList "/I C:\fredos\tmp\admx.msi";
            Copy-Item -Path "C:\Program Files (x86)\Microsoft Group Policy\Windows Server 2022 August 2021 Update\PolicyDefinitions\*" -Destination "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions" -Recurse;
            Write-Host "admx builded." -ForegroundColor "green";
        } else {
            Write-Host "admx already builded." -ForegroundColor "green";
        }
    }
}
function fnBackup() {
    ### Backup
    # src : http://woshub.com/backup-active-directory-domain-controller/
    If (-NOT (Test-Path 'HKLM:\SYSTEM\CurrentControlSet\Services\wbengine\SystemStateBackup')) {New-Item -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\wbengine\SystemStateBackup' -Force | Out-Null}
    Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\wbengine\SystemStateBackup' -Name 'AllowSSBToAnyVolume' -Value '1' -Force;
    New-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\wbengine\SystemStateBackup' -Name 'AllowSSBToAnyVolume' -Value '1' -PropertyType 'DWORD' -Force;
    Import-Module ServerManager;
    $TargetUNC = "C:\fredos\backup\$(Get-Date -Format 'yyyyMMdd_HHmm')";
    $TestTargetUNC = Test-Path -Path $TargetUNC;
    if (!($TestTargetUNC)){New-Item -Path $TargetUNC -ItemType directory;}
    $WBadmin_cmd = "wbadmin start backup -backupTarget:B: -include:C: -noverify -vssCopy -quiet";
    Invoke-Expression $WBadmin_cmd;
}

##### menu
# src : https://wiki-tech.io/Scripting/Powershell/Menu
$continue = $true
if ($GetOS -eq "desktop") {
    while ($continue){
        Write-Host "[ FredOS ]" -BackgroundColor DarkBlue -ForegroundColor White -NoNewline
        Write-Host " $GetDate :: starting $GetOS mode...`n" -ForegroundColor Red
        write-host " ---------- BASIC STUFF ----------"
        write-host " [1] (PowerShell 5) full FRA, time fix, all Upgrades, install remarkables packages"
        write-host " [2] (Powershell 7) downloads, autostarts, no CTRL/ALT, show files extensions, show hidden files, left taskbar, old contextual menu, old explorer ribbon, disable weather widget, disable mapsbroker, enable verr num & FredOS wallpaper"
        write-host ""
        write-host " ---------- CLIENT ----------"
        write-host " [3] install more packages"
        write-host " [4] rename to '$GetName'"
        write-host " [5] join '$GetDomainFull'"
        write-host ""
        write-host " ---------- SCRIPT ----------"
        write-host " [x] exit"
        write-host " [s] shutdown"
        write-host " [r] reboot"
        write-host ""
        $choice = read-host " make choice :"
        switch ($choice) {
            1{fnBasics1}
            2{fnBasics2}
            3{fnInstallMore}
            4{fnRename}
            5{fnJoinDomain}
            'x' {$continue = $false}
            's'{Stop-Computer}
            'r'{Restart-Computer}
            default {Write-Host " invalid choice."-ForegroundColor Red}
        }
    }
} else {
    while ($continue){
        Write-Host "[ FredOS ]" -BackgroundColor DarkBlue -ForegroundColor White -NoNewline
        Write-Host " $GetDate :: starting $GetOS mode...`n" -ForegroundColor Red
        write-host " ---------- BASIC STUFF ----------"
        write-host " [1] (PowerShell 5) full FRA, time fix, all Upgrades & install remarkables packages"
        write-host " [2] (Powershell 7) downloads, autostarts, no CTRL/ALT, show files extensions, show hidden files, left taskbar, old contextual menu, old explorer ribbon, disable weather widget, disable mapsbroker, enable verr num, FredOS wallpaper & LAPS"
        write-host ""
        write-host " ---------- SERVER $GetDomainFull (config) ----------"
        write-host " [3] activation & rename to '$GetName'"
        write-host " [4] config network"
        write-host " [5] install AD-DS + DNS + WSB"
        write-host " [6] install DHCP"
        write-host " [7] install Routing"
        write-host " [8] install IIS"
        write-host ""
        write-host " ---------- SERVER $GetDomainFull (datas) ----------"
        write-host " [9] add accounts, groups & ous"
        write-host " [10] add GPOs"
        write-host " [11] add ADMXs"
        write-host " [12] backup"
        write-host " [13] protect AD w. PingCastle & Purple Knight best practices"
        write-host ""
        write-host " ---------- SCRIPT ----------"
        write-host " [x] exit"
        write-host " [s] shutdown"
        write-host " [r] reboot"
        write-host ""
        $choice = read-host " make choice :"
        switch ($choice) {
            1{fnBasics1}
            2{fnBasics2}
            3{fnRename}
            4{fnConfigNetwork}
            5{fnConfigAD}
            6{fnConfigDHCP}
            7{fnConfigROuting}
            8{fnConfigIIS}
            9{fnFeedADaccounts}
            10{fnFeedADgpos}
            11{fnADMX}
            12{fnBackup}
            13{fnProtectAD}
            'x' {$continue = $false}
            's'{Stop-Computer}
            'r'{Restart-Computer}
            default {Write-Host " invalid choice."-ForegroundColor Red}
        }
    }
}
