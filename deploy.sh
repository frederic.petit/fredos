#!/bin/bash
############################
#   ___                 ____             _
#  / _ \__   _____ _ __|  _ \  ___ _ __ | | ___  _   _ 
# | | | \ \ / / _ \ '__| | | |/ _ \ '_ \| |/ _ \| | | |
# | |_| |\ V /  __/ |  | |_| |  __/ |_) | | (_) | |_| |
#  \___/  \_/ \___|_|  |____/ \___| .__/|_|\___/ \__, |
#                                 |_|            |___/ 
############################

declare -A APP;
APP[NAME]="OverDeploy";
APP[VERSION]="2.0.0-DEV";
APP[AUTHOR]="Frédéric Petit <contact@fredericpetit.fr>";

function usage {
cat <<EOUSAGE

SYNOPSIS
    ${APP[NAME]} - GNU/Linux cuztomisations for distributions Debian 11/12, Ubuntu 22.04 & Fedora 37 with GNOME/KDE/XFCE. Goals here are to provide a system ready for learn IT support and some basics development tools. Datas are provided from JSON files with desktop or server profiles (not available here). Get the script, launch it and wait for the magic ! This is a Cloud-Init inspiration but can working with her, Syslinux or on current installation, in bash.

REQUIREMENTS
    - root rights
    - GIT & JQ programs
	
UTILISATION
    - script : $(basename $0) [ GIT SOURCE /json ] [ CLI / DE ] [ SOFTCORE / SOFTWARE ] [ PRIORITY ] [ GIT SOURCE /pool ]
    - example : apt-get update && apt-get install git -y; token="000"; git clone --depth=1 --branch=main https://gitlab.com/fredericpetit/fredos.git /root/fredos && chmod +x /root/fredos/src/linux/*.sh && chmod +x /root/fredos/deploy.sh && /bin/bash /root/fredos/deploy.sh "auto" "auto" "auto" "1" "auto"; rm -R -f /root/fredos;
    - package : sudo dpkg -i fredos.deb && fredos "auto" "xfce" "software" "0" "auto"
	
PARAMETERS
    - GIT SOURCE /json
        source for .json datas files
	- GIT SOURCE /pool
        source for plain content files (original or vendor)
    - CLI / DE
        choice beetween CLI or Desktop Environnement
    - SOFTCORE / SOFTWARE
        choice beetween modes Softcore, Software or both
    - PRIORITY
        choice beetween few .json files

TO-DO
    - urgent : check urls with nginx proxy pass mode, finish colors to array
    - later : check nginx service for icon, add custom ssh config/user keys, type of skels by a status not a name, verif after download ?

NOTES
    - jqplay : downgrade go version, add godotenv, load godotenv, add HTML doctype & clean HTML
    - EOUSAGE https://linuxhint.com/what-is-cat-eof-bash-script/

METADATA
    - Author : ${APP[AUTHOR]}
    - Version : ${APP[VERSION]}

.
EOUSAGE
}

##### Globals VARS
# Others globals vars are :
#   - scriptArg(1/2/3/4) (previously defined)
#   - varDesktopEnvironnemment (defined in CHECKS)
#   - varData (defined in CHECKS)
#   - the fancy colors defined in colors.sh
declare -A CONF
CONF[TIMEOUT]="20"


scriptPath=$(dirname $(readlink -f $0));
scriptDefaults=("auto" "auto" "auto" "0" "auto");
if [[ -n "$1" ]]; then scriptArg1="$1"; else scriptArg1="${scriptDefaults[0]}"; fi;
if [[ -n "$2" ]]; then scriptArg2="$2"; else scriptArg2="${scriptDefaults[1]}"; fi;
if [[ -n "$3" ]]; then scriptArg3="$3"; else scriptArg3="${scriptDefaults[2]}"; fi;
if [[ -n "$4" ]]; then scriptArg4="$4"; else scriptArg4="${scriptDefaults[3]}"; fi;
if [[ -n "$5" ]]; then scriptArg5="$5"; else scriptArg5="${scriptDefaults[4]}"; fi;
for file in ${scriptPath}/src/linux/*; do source "$file"; done;
scriptSrcJson="https://gitlab.com/fredericpetit/fredos-datas.git";
scriptSrcPool="https://gitlab.com/fredericpetit/fredos-pool.git";
scriptDateStart="";
scriptDateFinished="";

##### Globals FUNCTIONS
# fnCheckDir() - for multiple
#
#   ARGS :
#   - $1 :
#       - string dirpath
#   - $2 :
#       - boolean create if not exists
#   - $3 :
#       - string chmod
#   - $4 :
#       - string chown
#   - $5 :
#       - string chattr
#
#   RETURN : code
#
function fnCheckDir() {
    if [[ -n "$1" ]]; then
        if [[ "$2" = true ]]; then
            if mkdir -p "$1" &> /dev/null; then
                if [[ -n "$3" && "$3" != false && "$3" != "null" ]]; then chmod -R "$3" "$1"; fi;
                if [[ -n "$4" && "$4" != false && "$4" != "null" ]]; then chown -R "$4" "$1"; fi;
                if [[ -n "$5" && "$5" != false && "$5" != "null" ]]; then chattr +i "$1"; fi;
                return 0;
            else return 1; fi;
        fi;
        if [ -d "$1" &> /dev/null ]; then
            if [[ -n "$3" && "$3" != false && "$3" != "null" ]]; then chmod -R "$3" "$1"; fi;
            if [[ -n "$4" && "$4" != false && "$4" != "null" ]]; then chown -R "$4" "$1"; fi;
            if [[ -n "$5" && "$5" != false && "$5" != "null" ]]; then chattr +i "$1"; fi;
            return 0;
        else return 1; fi;
    else return 1; fi;
}

# fnCheckFile() - for multiple
#
#   ARGS :
#   - $1 :
#       - string filepath
#   - $2 :
#       - boolean create if not exists
#   - $3 :
#       - string chmod
#   - $4 :
#       - string chown
#   - $5 :
#       - string chattr
#
#   RETURN : code
#
function fnCheckFile() {
    if [[ -n "$1" ]]; then
        if [[ "$2" = true ]]; then
            if touch "$1" &> /dev/null; then
                if [[ -n "$3" && "$3" != false && "$3" != "null" ]]; then chmod "$3" "$1"; fi;
                if [[ -n "$4" && "$4" != false && "$4" != "null" ]]; then chown "$4" "$1"; fi;
                if [[ -n "$5" && "$5" != false && "$5" != "null" ]]; then chattr +i "$1"; fi;
                return 0;
            else return 1; fi;
        fi;
        if [ -f "$1" &> /dev/null ]; then
            if [[ -n "$3" && "$3" != false && "$3" != "null" ]]; then chmod "$3" "$1"; fi;
            if [[ -n "$4" && "$4" != false && "$4" != "null" ]]; then chown "$4" "$1"; fi;
            if [[ -n "$5" && "$5" != false && "$5" != "null" ]]; then chattr +i "$1"; fi;
            return 0;
        else return 1; fi;
    else return 1; fi;
}



###########################################################################
#
#  ██████ ██   ██ ███████  ██████ ██   ██ ███████ 
# ██      ██   ██ ██      ██      ██  ██  ██      
# ██      ███████ █████   ██      █████   ███████ 
# ██      ██   ██ ██      ██      ██  ██       ██ 
#  ██████ ██   ██ ███████  ██████ ██   ██ ███████  
#
###########################################################################
function fnCHECKS() {

    # fnFirstEcho() - welcoming
    function fnFirstEcho() {
        echo -e "
          ${COLOR[GREEN_LIGHT]}   ___                ${COLOR[ORANGE_DARK]} ____             _
          ${COLOR[GREEN_LIGHT]}  / _ \__   _____ _ __${COLOR[ORANGE_DARK]}|  _ \  ___ _ __ | | ___  _   _ 
          ${COLOR[GREEN_LIGHT]} | | | \ \ / / _ \ '__${COLOR[ORANGE_DARK]}| | | |/ _ \ '_ \| |/ _ \| | | |
          ${COLOR[GREEN_LIGHT]} | |_| |\ V /  __/ |  ${COLOR[ORANGE_DARK]}| |_| |  __/ |_) | | (_) | |_| |
          ${COLOR[GREEN_LIGHT]}  \___/  \_/ \___|_|  ${COLOR[ORANGE_DARK]}|____/ \___| .__/|_|\___/ \__, |
          ${COLOR[GREEN_LIGHT]}                      ${COLOR[ORANGE_DARK]}           |_|            |___/ 
        "
        fnEcho "Bienvenue sur ${APP[NAME]} (v${APP[VERSION]}) Déploiement automatisé,\n          Un script unique et spécial prenant en charge les distributions GNU/Linux Debian 11 & 12, Ubuntu 22.04 & Fedora 37,\n          avec les environnement GNOME, KDE & XFCE !\n          Il peut être lancé via cloud-init, Cubic/Syslinux customisations ou les installations fraîches.\n          Lancez-le simplement et revenez après un avoir pris un café ou un thé.\n          Votre système d'exploitation sera alors pimpé comme jamais.\n"
        scriptDateStart=$(date +%s)
    }
    # fnCheckOS() - Check OS call Get OS
    function fnCheckOS() {
        fnEcho "checking OS ..." "check"
        if [[ -n $(fnGetOS) && $(fnGetOS) ]]; then
            local os=$(fnGetOS)
            if [[ -n $(fnGetOSVersion) && $(fnGetOSVersion) ]]; then
                local osVersion=$(fnGetOSVersion)
            else
                fnEcho "impossible to find a compatible ${os^} version. exit." "checked" "fail"; exit 1;
            fi
            fnEcho "OS ${os^} ${osVersion} here." "checked" "success"
        else
            fnEcho "impossible to find a compatible OS. exit." "checked" "fail"; exit 1;
        fi
    }
    # fnCheckRoot() - Check root call Get root
    function fnCheckRoot() {
        fnEcho "checking root ..." "check"
        if $(fnGetRoot); then fnEcho "root here." "checked" "success"; else fnEcho "root needed. exit." "checked" "fail"; exit 1; fi;
    }
    # fnCheckArgs() - Check arguments of the script
    function fnCheckArgs() {
        fnEcho "script arguments are :" "checked" "info"
        fnEcho "source install : checking argument #1 ..." "check"
        if [[ -n $(fnGetArg1) && $(fnGetArg1) ]]; then
            if [[ $(fnGetArg1) == "${scriptSrcJson}" ]]; then local fnGetArg1Text="auto"; else local fnGetArg1Text="$(fnGetArg1 | sed -e 's/oauth2:\(.*\)@//g')"; fi;
            fnEcho "source /json install : '${BOLD}${fnGetArg1Text}${NOCOLOR}'." "checked" "success"
        else
            fnEcho "source /json install : argument #1 not valid" "checked" "fail"; exit 1;
        fi
        fnEcho "type install : checking argument #2 ..." "check"
        if [[ -n $(fnGetArg2) && $(fnGetArg2) ]]; then
            fnEcho "type install : '${BOLD}$(fnGetArg2)${NOCOLOR}'." "checked" "success"
        else
            fnEcho "type install : argument #2 not valid" "checked" "fail"; exit 1;
        fi
        fnEcho "stuff install : checking argument #3 ..." "check"
        if [[ -n $(fnGetArg3) && $(fnGetArg3) ]]; then
            fnEcho "stuff install : '${BOLD}$(fnGetArg3)${NOCOLOR}'." "checked" "success"
        else
            fnEcho "stuff install : argument #3 not valid" "checked" "fail"; exit 1;
        fi
        fnEcho "priority install : checking argument #4 ..." "check"
        if [[ -n $(fnGetArg4) && $(fnGetArg4) ]]; then
            fnEcho "priority install : '${BOLD}$(fnGetArg4)${NOCOLOR}'." "checked" "success"
        else
            fnEcho "priority install : argument #4 not valid" "checked" "fail"; exit 1;
        fi
        if [[ -n $(fnGetArg5) && $(fnGetArg5) ]]; then
            if [[ $(fnGetArg5) == "${scriptSrcPool}" ]]; then local fnGetArg5Text="auto"; else local fnGetArg5Text="$(fnGetArg5 | sed -e 's/oauth2:\(.*\)@//g')"; fi;
            fnEcho "source /pool install : '${BOLD}${fnGetArg5Text}${NOCOLOR}'." "checked" "success"
        else
            fnEcho "source /pool install : argument #5 not valid" "checked" "fail"; exit 1;
        fi
    }
    # fnCheckGitJson() - Download GIT repo
    function fnCheckGitJson() {
        fnEcho "[i] checking JSON datas deploy ..."
        if fnGetDownload "git" "$(fnGetArg1)" "${scriptPath}/fredos-datas"; then
            fnEcho "JSON datas downloaded." "checked" "success"
        else
            fnEcho "JSON datas not downloadable. exit." "checked" "fail"; exit 1;
        fi
        fnEcho "[i] JSON datas deploy checked ..."
    }
    # fnGetCurrentDE() - Get current DE with packages installed with priority order
    function fnGetCurrentDE() {
        if [[ $1 = "task-gnome-desktop" || $1 = "ubuntu-desktop" || $1 = "gnome-shell" ]]; then
            echo "gnome"
        elif [[ $1 = "task-kde-desktop" || $1 = "kubuntu-desktop" || $1 = "kde-settings" ]]; then
            echo "kde"
        elif [[ $1 = "task-xfce-desktop" || $1 = "xubuntu-desktop" || $1 = "xfce-desktop-environment" ]]; then
            echo "xfce"
        else
            return 1
        fi
    }
    # fnFetchCurrentDE() - Fetch (the current, not the one wanted) DE by his package for fnCheckCurrentDE()
    function fnFetchCurrentDE() {
        if [[ $(fnGetOS) = "debian" ]]; then
            local arrayDebian=("task-gnome-desktop task-kde-desktop task-xfce-desktop")
            for environnement in ${arrayDebian[@]}; do
                if $(fnCheckPackage "$environnement"); then echo $(fnGetCurrentDE "${environnement}"); break; fi;
            done
            return 1
        elif [[ $(fnGetOS) = "ubuntu" ]]; then
            arrayUbuntu=("ubuntu-desktop kubuntu-desktop xubuntu-desktop")
            for environnement in ${arrayUbuntu[@]}; do
                if $(fnCheckPackage "$environnement"); then echo $(fnGetCurrentDE "${environnement}"); break; fi;
            done
            return 1
        elif [[ $(fnGetOS) = "fedora" ]]; then
            arrayFedora=("gnome-shell kde-settings xfce-desktop-environment")
            for environnement in ${arrayFedora[@]}; do
                if $(fnCheckPackage "$environnement"); then echo $(fnGetCurrentDE "${environnement}"); break; fi;
            done
            return 1
        fi
        return 1
    }
    # fnCheckCurrentDE() - call fnFetchCurrentDE()
    function fnCheckCurrentDE() {
        fnEcho "checking current Desktop Environnement if present ..." "check"
        if [[ -n $(fnFetchCurrentDE) && $(fnFetchCurrentDE) ]]; then
            local current=$(fnFetchCurrentDE)
            if [[ $current != "server" ]]; then
                fnEcho "${BOLD}${current^}${NOCOLOR} Desktop Environnement available." "checked" "success"
            else
                fnEcho "there is no Desktop Environnement available." "checked" "success"
            fi
        else
            fnEcho "current Desktop Environnement not definable. exit." "checked" "fail"; exit 1;
        fi
    }
    # fnPrepareDeploy() - Make final decision for DE
    function fnPrepareDeploy() {
        local current=$(fnFetchCurrentDE)
        local wanted=$(fnGetArg2)
        local prepareText="prepare to parse JSON deploy configuration"
        fnEcho "checking future deploy for stuffs current (${current}) and wanted (${wanted}) ..." "check"
        if [[ $wanted = "server" ]]; then
            varDesktopEnvironnemment=false
            fnEcho "${prepareText} without Desktop Environnement ..." "checked" "info"
        elif [[ $wanted = "auto" ]]; then
            if [[ $current = "gnome" || $current = "kde" || $current = "xfce" ]]; then
                varDesktopEnvironnemment=${current}
                fnEcho "${prepareText} with Desktop Environnement ${varDesktopEnvironnemment^} ..." "checked" "info"
            else
                varDesktopEnvironnemment=false
                fnEcho "${prepareText} without Desktop Environnement ..." "checked" "info"
            fi
        elif [[ $wanted = "desktop" ]]; then
            if [[ $current = "kde" || $current = "xfce" ]]; then
                varDesktopEnvironnemment=${current}
                fnEcho "${prepareText} with Desktop Environnement ${varDesktopEnvironnemment^} ..." "checked" "info"
            else
                varDesktopEnvironnemment="gnome"
                fnEcho "${prepareText} with Desktop Environnement ${varDesktopEnvironnemment^} ..." "checked" "info"
            fi
        elif [[ $wanted = "gnome" || $wanted = "kde" || $wanted = "xfce" ]]; then
            varDesktopEnvironnemment=${wanted}
            fnEcho "${prepareText} with Desktop Environnement ${varDesktopEnvironnemment^} ..." "checked" "info"
        else
            fnEcho "impossible to prepare to parse JSON deploy configuration. exit." "checked" "fail"; exit 1;
        fi
    }
    # fnGetDeploy() - Define varData
    function fnGetDeploy() {
        local osName="$(cut -d'_' -f1 <<< $(basename ${1}))"
        local osVersion="$(cut -d'_' -f2 <<< $(basename ${1}))"
        local priority="$(cut -d'_' -f3 <<< $(basename $1 | sed -e "s/\.json//g"))"
        if [[ "${osName}" == "$(fnGetOS)" && "${osVersion}" == "$(fnGetOSVersion | sed -e "s/\.//g")" && "${priority}" == $(fnGetArg4) ]]; then varData=$(cat ${1} | jq); return 0; else return 1; fi;
    }
    # fnParseDeploy() - call fnGetDeploy()
    function fnParseDeploy() {
        fnEcho "parsing JSON deploy configuration ..." "check";
        if [[ -n $varDesktopEnvironnemment ]]; then
            for file in ${scriptPath}/fredos-datas/src/desktop/*.json; do
                if fnGetDeploy "${file}"; then break; fi;
            done
        else
            for file in ${scriptPath}/fredos-datas/src/server/*.json; do
                if fnGetDeploy "${file}"; then break; fi;
            done
        fi
        if [[ -n $varData ]]; then
            local type=$(echo $varData | jq -r ".system.type")
            local distribution=$(echo $varData | jq -r ".system.distribution")
            fnEcho "JSON deploy configuration finded for this ${distribution^} ${type}." "checked" "success";
        else
            fnEcho "JSON deploy configuration not finded." "checked" "fail"; exit 1;
        fi
    }
    # fnCheckDeployDepends() - Check dependencies (placeholder if need to separate package name / command name)
    function fnCheckDeployDepends() {
        local arrayDependencies=$(echo $varData | jq ".system.dependencies | keys | .[]")
        if [[ -n $arrayDependencies ]]; then
            local dependenciesNames="'$(echo "$varData" | jq --compact-output --raw-output '.system.dependencies | values | join("'\'', '\''")')'"
            fnEcho "[i] needed dependencies are ${BOLD}${dependenciesNames}${NOCOLOR}.";
            # loop files
            for dependence in ${arrayDependencies}; do
                value=$(echo $varData | jq -r -c ".system.dependencies[$dependence]")
                if [[ $value != "jq" ]]; then
                    fnCheckPrg $value $value
                fi
            done
        else
            fnEcho "[i] no needed dependencies."
        fi
    }
    function fnStart() {
        fnCheckOS
        fnCheckRoot
        echo "-------------------------"
        fnCheckArgs
        echo "-------------------------"
        fnCheckUpdatePackage
        echo "-------------------------"
        fnCheckUpgradePackage
        echo "-------------------------"
        fnCheckPrg "jq" "jq"
        echo "-------------------------"
        fnCheckGitJson
        echo "-------------------------"
        fnCheckCurrentDE
        fnPrepareDeploy
        fnParseDeploy
        echo "-------------------------"
        fnCheckDeployDepends
    }
    function fnRecapMsg() {
        local type=$(echo $varData | jq -r ".system.type")
        local distribution=$(echo $varData | jq -r ".system.distribution")
        local version=$(echo $varData | jq -r ".system.version")
        if [[ -n $varDesktopEnvironnemment ]]; then local desktop=" (${varDesktopEnvironnemment^} environnement)"; else local desktop=""; fi;
        fnEcho "Deployment initiated by $(fnGetUser) is ready for this ${type} system on ${distribution^} ${version}${desktop}." "checked" "info"
    }

    ##### CALLS #####
    fnFirstEcho
    fnEcho "CHECKS" "stage" "start"
    fnStart
    echo "-------------------------"
    fnRecapMsg
    fnEcho "CHECKS" "stage" "end"
    fnStages
}



###########################################################################
#
# ███████ ███    ██ ██████  
# ██      ████   ██ ██   ██ 
# █████   ██ ██  ██ ██   ██ 
# ██      ██  ██ ██ ██   ██ 
# ███████ ██   ████ ██████  
#
###########################################################################
function fnEND() {

    function fnForget() {                                                              
        history -c
    }
    ##### CALLS #####
    fnForget
    scriptDateFinished="$(expr $(date +%s) - ${scriptDateStart})"
    fnEcho "DEPLOY finished at $(fnGetDate) (script executed in $((${scriptDateFinished}%3600/60))m:$((${scriptDateFinished}%60))s)."
}



##### Everything starting here ... #####
fnCHECKS